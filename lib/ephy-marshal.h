
#ifndef __ephy_marshal_MARSHAL_H__
#define __ephy_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* BOOLEAN:BOXED (./ephy-marshal.list:1) */
extern void ephy_marshal_BOOLEAN__BOXED (GClosure     *closure,
                                         GValue       *return_value,
                                         guint         n_param_values,
                                         const GValue *param_values,
                                         gpointer      invocation_hint,
                                         gpointer      marshal_data);

/* BOOLEAN:ENUM,STRING,STRING,STRING (./ephy-marshal.list:2) */
extern void ephy_marshal_BOOLEAN__ENUM_STRING_STRING_STRING (GClosure     *closure,
                                                             GValue       *return_value,
                                                             guint         n_param_values,
                                                             const GValue *param_values,
                                                             gpointer      invocation_hint,
                                                             gpointer      marshal_data);

/* BOOLEAN:OBJECT (./ephy-marshal.list:3) */
extern void ephy_marshal_BOOLEAN__OBJECT (GClosure     *closure,
                                          GValue       *return_value,
                                          guint         n_param_values,
                                          const GValue *param_values,
                                          gpointer      invocation_hint,
                                          gpointer      marshal_data);

/* BOOLEAN:STRING (./ephy-marshal.list:4) */
extern void ephy_marshal_BOOLEAN__STRING (GClosure     *closure,
                                          GValue       *return_value,
                                          guint         n_param_values,
                                          const GValue *param_values,
                                          gpointer      invocation_hint,
                                          gpointer      marshal_data);

/* BOOLEAN:STRING,BOOLEAN,BOOLEAN (./ephy-marshal.list:5) */
extern void ephy_marshal_BOOLEAN__STRING_BOOLEAN_BOOLEAN (GClosure     *closure,
                                                          GValue       *return_value,
                                                          guint         n_param_values,
                                                          const GValue *param_values,
                                                          gpointer      invocation_hint,
                                                          gpointer      marshal_data);

/* BOOLEAN:STRING,STRING (./ephy-marshal.list:6) */
extern void ephy_marshal_BOOLEAN__STRING_STRING (GClosure     *closure,
                                                 GValue       *return_value,
                                                 guint         n_param_values,
                                                 const GValue *param_values,
                                                 gpointer      invocation_hint,
                                                 gpointer      marshal_data);

/* BOOLEAN:STRING,STRING,STRING (./ephy-marshal.list:7) */
extern void ephy_marshal_BOOLEAN__STRING_STRING_STRING (GClosure     *closure,
                                                        GValue       *return_value,
                                                        guint         n_param_values,
                                                        const GValue *param_values,
                                                        gpointer      invocation_hint,
                                                        gpointer      marshal_data);

/* BOOLEAN:VOID (./ephy-marshal.list:8) */
extern void ephy_marshal_BOOLEAN__VOID (GClosure     *closure,
                                        GValue       *return_value,
                                        guint         n_param_values,
                                        const GValue *param_values,
                                        gpointer      invocation_hint,
                                        gpointer      marshal_data);

/* OBJECT:OBJECT,FLAGS (./ephy-marshal.list:9) */
extern void ephy_marshal_OBJECT__OBJECT_FLAGS (GClosure     *closure,
                                               GValue       *return_value,
                                               guint         n_param_values,
                                               const GValue *param_values,
                                               gpointer      invocation_hint,
                                               gpointer      marshal_data);

/* OBJECT:STRING,OBJECT,FLAGS (./ephy-marshal.list:10) */
extern void ephy_marshal_OBJECT__STRING_OBJECT_FLAGS (GClosure     *closure,
                                                      GValue       *return_value,
                                                      guint         n_param_values,
                                                      const GValue *param_values,
                                                      gpointer      invocation_hint,
                                                      gpointer      marshal_data);

/* STRING:STRING,STRING (./ephy-marshal.list:11) */
extern void ephy_marshal_STRING__STRING_STRING (GClosure     *closure,
                                                GValue       *return_value,
                                                guint         n_param_values,
                                                const GValue *param_values,
                                                gpointer      invocation_hint,
                                                gpointer      marshal_data);

/* STRING:VOID (./ephy-marshal.list:12) */
extern void ephy_marshal_STRING__VOID (GClosure     *closure,
                                       GValue       *return_value,
                                       guint         n_param_values,
                                       const GValue *param_values,
                                       gpointer      invocation_hint,
                                       gpointer      marshal_data);

/* VOID:ENUM (./ephy-marshal.list:13) */
#define ephy_marshal_VOID__ENUM	g_cclosure_marshal_VOID__ENUM

/* VOID:POINTER,BOOLEAN (./ephy-marshal.list:14) */
extern void ephy_marshal_VOID__POINTER_BOOLEAN (GClosure     *closure,
                                                GValue       *return_value,
                                                guint         n_param_values,
                                                const GValue *param_values,
                                                gpointer      invocation_hint,
                                                gpointer      marshal_data);

/* VOID:POINTER,POINTER (./ephy-marshal.list:15) */
extern void ephy_marshal_VOID__POINTER_POINTER (GClosure     *closure,
                                                GValue       *return_value,
                                                guint         n_param_values,
                                                const GValue *param_values,
                                                gpointer      invocation_hint,
                                                gpointer      marshal_data);

/* VOID:STRING,FLAGS (./ephy-marshal.list:16) */
extern void ephy_marshal_VOID__STRING_FLAGS (GClosure     *closure,
                                             GValue       *return_value,
                                             guint         n_param_values,
                                             const GValue *param_values,
                                             gpointer      invocation_hint,
                                             gpointer      marshal_data);

/* VOID:STRING,INT (./ephy-marshal.list:17) */
extern void ephy_marshal_VOID__STRING_INT (GClosure     *closure,
                                           GValue       *return_value,
                                           guint         n_param_values,
                                           const GValue *param_values,
                                           gpointer      invocation_hint,
                                           gpointer      marshal_data);

/* VOID:STRING,STRING (./ephy-marshal.list:18) */
extern void ephy_marshal_VOID__STRING_STRING (GClosure     *closure,
                                              GValue       *return_value,
                                              guint         n_param_values,
                                              const GValue *param_values,
                                              gpointer      invocation_hint,
                                              gpointer      marshal_data);

/* VOID:STRING,STRING,STRING (./ephy-marshal.list:19) */
extern void ephy_marshal_VOID__STRING_STRING_STRING (GClosure     *closure,
                                                     GValue       *return_value,
                                                     guint         n_param_values,
                                                     const GValue *param_values,
                                                     gpointer      invocation_hint,
                                                     gpointer      marshal_data);

G_END_DECLS

#endif /* __ephy_marshal_MARSHAL_H__ */

