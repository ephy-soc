/*
 *  Copyright © 2000-2003 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 *  $Id: ephy-dialog.h 6952 2007-03-11 19:42:02Z chpe $
 */

#ifndef EPHY_DIALOG_H
#define EPHY_DIALOG_H

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtkwidget.h>

G_BEGIN_DECLS

#define EPHY_TYPE_DIALOG		(ephy_dialog_get_type ())
#define EPHY_DIALOG(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), EPHY_TYPE_DIALOG, EphyDialog))
#define EPHY_DIALOG_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), EPHY_TYPE_DIALOG, EphyDialogClass))
#define EPHY_IS_DIALOG(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), EPHY_TYPE_DIALOG))
#define EPHY_IS_DIALOG_CLASS(k)		(G_TYPE_CHECK_CLASS_TYPE ((k), EPHY_TYPE_DIALOG))
#define EPHY_DIALOG_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), EPHY_TYPE_DIALOG, EphyDialogClass))

typedef struct _EphyDialogClass		EphyDialogClass;
typedef struct _EphyDialog		EphyDialog;
typedef struct _EphyDialogPrivate	EphyDialogPrivate;

typedef enum
{
	PT_NORMAL	= 0,
	PT_AUTOAPPLY	= 1 << 0,
	PT_INVERTED	= 1 << 1
} EphyDialogApplyType;

typedef struct
{
	const char *id;
	const char *pref;
	EphyDialogApplyType apply_type;
	GType data_type;
} EphyDialogProperty;

struct _EphyDialogClass
{
	GObjectClass parent_class;

	/* Signals */

	void	(* changed)	(EphyDialog *dialog,
				 const GValue *value);

	/* Methods */
	void	(* construct)	(EphyDialog *dialog,
				 const EphyDialogProperty *properties,
				 const char *file,
				 const char *name,
				 const char *domain);
	void	(* show)	(EphyDialog *dialog);
};

struct _EphyDialog
{
	GObject parent;

	/*< private >*/
	EphyDialogPrivate *priv;
};

GType		ephy_dialog_get_type		(void);

EphyDialog     *ephy_dialog_new			(void);

EphyDialog     *ephy_dialog_new_with_parent	(GtkWidget *parent_window);

void		ephy_dialog_construct		(EphyDialog *dialog,
						 const EphyDialogProperty *properties,
						 const char *file,
						 const char *name,
						 const char *domain);

void		ephy_dialog_add_enum		(EphyDialog *dialog,
						 const char *id,
						 guint n_items,
						 const char * const *items);

void		ephy_dialog_set_data_column	(EphyDialog *dialog,
						 const char *id,
						 int col);

void		ephy_dialog_set_size_group	(EphyDialog *dialog,
						 const char *first_id,
						 ...);

int		ephy_dialog_run			(EphyDialog *dialog);

void		ephy_dialog_show		(EphyDialog *dialog);

void		ephy_dialog_hide		(EphyDialog *dialog);

void		ephy_dialog_set_parent		(EphyDialog *dialog,
						 GtkWidget *parent);

GtkWidget      *ephy_dialog_get_parent		(EphyDialog *dialog);

void		ephy_dialog_set_modal		(EphyDialog *dialog,
						 gboolean is_modal);

GtkWidget      *ephy_dialog_get_control		(EphyDialog *dialog,
						 const char *property_id);

void		ephy_dialog_get_controls	(EphyDialog *dialog,
						 const char *first_property_id,
						 ...);

gboolean	ephy_dialog_get_value		(EphyDialog *dialog,
						 const char *property_id,
						 GValue *value);

void		ephy_dialog_set_value		(EphyDialog *dialog,
						 const char *property_id,
						 const GValue *value);

void		ephy_dialog_set_pref		(EphyDialog *dialog,
						 const char *property_id,
						 const char *pref);

G_END_DECLS

#endif
