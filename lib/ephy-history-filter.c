/* 
 *  Copyright © 2002 Olivier Martin <omartin@ifrance.com>
 *            (C) 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 *  $Id: ephy-node-filter.c 6952 2007-03-11 19:42:02Z chpe $
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "ephy-history-filter.h"

static void ephy_history_filter_class_init (EphyHistoryFilterClass *klass);
static void ephy_history_filter_init (EphyHistoryFilter *node);
static void ephy_history_filter_finalize (GObject *object);
static gboolean ephy_history_filter_expression_evaluate (EphyHistoryFilterExpression *expression,
						      EphyNode *node);

enum
{
	CHANGED,
	LAST_SIGNAL
};

#define EPHY_HISTORY_FILTER_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), EPHY_TYPE_HISTORY_FILTER, EphyHistoryFilterPrivate))

struct _EphyHistoryFilterPrivate
{
	GPtrArray *levels;
};

struct _EphyHistoryFilterExpression
{
	EphyHistoryFilterExpressionType type;

	union
	{
		struct
		{
			EphyHistoryNode *a;
			EphyHistoryNode *b;
		} node_args;

		struct
		{
			int prop_id;

			union
			{
				EphyHistoryNode *node;
				char *string;
				int number;
			} second_arg;
		} prop_args;
	} args;
};

static GObjectClass *parent_class = NULL;

static guint ephy_history_filter_signals[LAST_SIGNAL] = { 0 };

GType
ephy_history_filter_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0))
	{
		const GTypeInfo our_info =
		{
			sizeof (EphyHistoryFilterClass),
			NULL,
			NULL,
			(GClassInitFunc) ephy_history_filter_class_init,
			NULL,
			NULL,
			sizeof (EphyHistoryFilter),
			0,
			(GInstanceInitFunc) ephy_history_filter_init
		};

		type = g_type_register_static (G_TYPE_OBJECT,
					       "EphyHistoryFilter",
					       &our_info, 0);
	}

	return type;
}

static void
ephy_history_filter_class_init (EphyHistoryFilterClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = ephy_history_filter_finalize;

	ephy_history_filter_signals[CHANGED] =
		g_signal_new ("changed",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (EphyHistoryFilterClass, changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE,
			      0);

	g_type_class_add_private (object_class, sizeof (EphyHistoryFilterPrivate));
}

static void
ephy_history_filter_init (EphyHistoryFilter *filter)
{
	filter->priv = EPHY_HISTORY_FILTER_GET_PRIVATE (filter);

	filter->priv->levels = g_ptr_array_new ();
}

static void
ephy_history_filter_finalize (GObject *object)
{
	EphyHistoryFilter *filter = EPHY_HISTORY_FILTER (object);

	ephy_history_filter_empty (filter);

	g_ptr_array_free (filter->priv->levels, TRUE);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

EphyHistoryFilter *
ephy_history_filter_new (void)
{
	return EPHY_HISTORY_FILTER (g_object_new (EPHY_TYPE_HISTORY_FILTER, NULL));
}

void
ephy_history_filter_add_expression (EphyHistoryFilter *filter,
			         EphyHistoryFilterExpression *exp,
			         int level)
{
	while (level >= filter->priv->levels->len)
		g_ptr_array_add (filter->priv->levels, NULL);

	/* FIXME bogosity! This only works because g_list_append (x, data) == x */
	g_ptr_array_index (filter->priv->levels, level) =
		g_list_append (g_ptr_array_index (filter->priv->levels, level), exp);
}

void
ephy_history_filter_empty (EphyHistoryFilter *filter)
{
	int i;
	
	for (i = filter->priv->levels->len - 1; i >= 0; i--)
	{
		GList *list, *l;

		list = g_ptr_array_index (filter->priv->levels, i);

		for (l = list; l != NULL; l = g_list_next (l))
		{
			EphyHistoryFilterExpression *exp;

			exp = (EphyHistoryFilterExpression *) l->data;

			ephy_history_filter_expression_free (exp);
		}

		g_list_free (list);

		g_ptr_array_remove_index (filter->priv->levels, i);
	}
}

void
ephy_history_filter_done_changing (EphyHistoryFilter *filter)
{
	g_signal_emit (G_OBJECT (filter), ephy_history_filter_signals[CHANGED], 0);
}

/*
 * We go through each level evaluating the filter expressions. 
 * Every time we get a match we immediately do a break and jump
 * to the next level. We'll return FALSE if we arrive to a level 
 * without matches, TRUE otherwise.
 */
gboolean
ephy_history_filter_evaluate (EphyHistoryFilter *filter,
			   EphyHistoryNode *node)
{
	int i;

	for (i = 0; i < filter->priv->levels->len; i++) {
		GList *l, *list;
		gboolean handled;

		handled = FALSE;

		list = g_ptr_array_index (filter->priv->levels, i);

		for (l = list; l != NULL; l = g_list_next (l)) {
			if (ephy_history_filter_expression_evaluate (l->data, node) == TRUE) {
				handled = TRUE;
				break;
			}
		}

		if (list != NULL && handled == FALSE)
			return FALSE;
	}
	
	return TRUE;
}

EphyHistoryFilterExpression *
ephy_history_filter_expression_new (EphyHistoryFilterExpressionType type,
			         ...)
{
	EphyHistoryFilterExpression *exp;
	va_list valist;

	va_start (valist, type);

	exp = g_new0 (EphyHistoryFilterExpression, 1);

	exp->type = type;

	switch (type)
	{
	case EPHY_HISTORY_FILTER_EXPRESSION_NODE_EQUALS:
		exp->args.node_args.a = va_arg (valist, EphyHistoryNode *);
		exp->args.node_args.b = va_arg (valist, EphyHistoryNode *);
		break;
	case EPHY_HISTORY_FILTER_EXPRESSION_EQUALS:
	case EPHY_HISTORY_FILTER_EXPRESSION_HAS_PARENT:
	case EPHY_HISTORY_FILTER_EXPRESSION_HAS_CHILD:
		exp->args.node_args.a = va_arg (valist, EphyHistoryNode *);
		break;
	case EPHY_HISTORY_FILTER_EXPRESSION_NODE_PROP_EQUALS:
	case EPHY_HISTORY_FILTER_EXPRESSION_CHILD_PROP_EQUALS:
		exp->args.prop_args.prop_id = va_arg (valist, int);
		exp->args.prop_args.second_arg.node = va_arg (valist, EphyHistoryNode *);
		break;
	case EPHY_HISTORY_FILTER_EXPRESSION_STRING_PROP_CONTAINS:
	case EPHY_HISTORY_FILTER_EXPRESSION_STRING_PROP_EQUALS:
		exp->args.prop_args.prop_id = va_arg (valist, int);
		exp->args.prop_args.second_arg.string = g_utf8_casefold (va_arg (valist, char *), -1);
		break;
	case EPHY_HISTORY_FILTER_EXPRESSION_KEY_PROP_CONTAINS:
	case EPHY_HISTORY_FILTER_EXPRESSION_KEY_PROP_EQUALS:
	{
		char *folded;

		exp->args.prop_args.prop_id = va_arg (valist, int);

		folded = g_utf8_casefold (va_arg (valist, char *), -1);
		exp->args.prop_args.second_arg.string = g_utf8_collate_key (folded, -1);
		g_free (folded);
		break;
	}
	case EPHY_HISTORY_FILTER_EXPRESSION_INT_PROP_EQUALS:
	case EPHY_HISTORY_FILTER_EXPRESSION_INT_PROP_BIGGER_THAN:
	case EPHY_HISTORY_FILTER_EXPRESSION_INT_PROP_LESS_THAN:
		exp->args.prop_args.prop_id = va_arg (valist, int);
		exp->args.prop_args.second_arg.number = va_arg (valist, int);
		break;
	default:
		break;
	}

	va_end (valist);

	return exp;
}

void
ephy_history_filter_expression_free (EphyHistoryFilterExpression *exp)
{
	switch (exp->type)
	{
	case EPHY_HISTORY_FILTER_EXPRESSION_STRING_PROP_CONTAINS:
	case EPHY_HISTORY_FILTER_EXPRESSION_STRING_PROP_EQUALS:
	case EPHY_HISTORY_FILTER_EXPRESSION_KEY_PROP_CONTAINS:
	case EPHY_HISTORY_FILTER_EXPRESSION_KEY_PROP_EQUALS:
		g_free (exp->args.prop_args.second_arg.string);
		break;
	default:
		break;
	}
	
	g_free (exp);
}

static gboolean
ephy_history_filter_expression_evaluate (EphyHistoryFilterExpression *exp,
				      EphyHistoryNode *node)
{
	switch (exp->type)
	{
	case EPHY_HISTORY_FILTER_EXPRESSION_ALWAYS_TRUE:
		return TRUE;
	case EPHY_HISTORY_FILTER_EXPRESSION_NODE_EQUALS:
		return (exp->args.node_args.a == exp->args.node_args.b);
	case EPHY_HISTORY_FILTER_EXPRESSION_EQUALS:
		return (exp->args.node_args.a == node);	
	/* XXX: we only handle the site->pages relationship
	 * split this case to handle more relations (e.g. page->visits) 
	 **/
	case EPHY_HISTORY_FILTER_EXPRESSION_HAS_PARENT: 
	{
		GValue *value;
		guint64 parent_id, site_id;

		ephy_history_node_get_property_by_id (node,
    					EPHY_HISTORY_PAGE_NODE_PROP_SITE_ID,
					&value);
		site_id = g_value_get_uint64 (value);
		g_value_unset (value);

		ephy_history_node_get_property_by_id (exp->args.node_args.a,
    						 EPHY_HISTORY_NODE_PROP_ID,
						 &value);
		parent_id = g_value_get_uint (value);
		g_value_unset (value);

		return (parent_id == site_id);
	}

/* XXX: these expressions are unused for now but need to re-written anyway 
	case EPHY_HISTORY_FILTER_EXPRESSION_HAS_CHILD:
		return ephy_node_has_child (node, exp->args.node_args.a);
	case EPHY_HISTORY_FILTER_EXPRESSION_CHILD_PROP_EQUALS:
	{
		EphyNode *prop;
		GPtrArray *children;
		int i;
		
		children = ephy_node_get_children (node);
		for (i = 0; i < children->len; i++)
		{
			EphyNode *child;
			
			child = g_ptr_array_index (children, i);
			prop = ephy_node_get_property_node 
				(child, exp->args.prop_args.prop_id);
		
			if (prop == exp->args.prop_args.second_arg.node)
			{
				return TRUE;
			}
		}
		
		return FALSE;
	}
*/
	case EPHY_HISTORY_FILTER_EXPRESSION_STRING_PROP_CONTAINS:
	{
		const char *prop;
		char *folded_case;
		gboolean ret;
		GValue *value;

		ephy_history_node_get_property_by_id (node, 
						 exp->args.prop_args.prop_id,
						 &value);
		prop = g_value_get_string (value);
	
		if (prop == NULL)
			return FALSE;

		folded_case = g_utf8_casefold (prop, -1);
		ret = (strstr (folded_case, exp->args.prop_args.second_arg.string) != NULL);
		g_free (folded_case);
		g_value_unset (value);

		return ret;
	}
	case EPHY_HISTORY_FILTER_EXPRESSION_STRING_PROP_EQUALS:
	{
		const char *prop;
		char *folded_case;
		gboolean ret;
		GValue *value;

		ephy_history_node_get_property_by_id (node, 
						 exp->args.prop_args.prop_id,
						 &value);
		prop = g_value_get_string (value);
	
		if (prop == NULL)
			return FALSE;

		folded_case = g_utf8_casefold (prop, -1);
		ret = (strcmp (folded_case, exp->args.prop_args.second_arg.string) == 0);
		g_free (folded_case);
		g_value_unset (value);

		return ret;
	}
	case EPHY_HISTORY_FILTER_EXPRESSION_KEY_PROP_CONTAINS:
	{
		const char *prop;
		GValue *value;
		gboolean ret;

		ephy_history_node_get_property_by_id (node, 
						 exp->args.prop_args.prop_id,
						 &value);
		prop = g_value_get_string (value);
	
		if (prop == NULL)
			return FALSE;

		ret = (strstr (prop, exp->args.prop_args.second_arg.string) != NULL);
		g_value_unset (value);
		return ret;
	}
	case EPHY_HISTORY_FILTER_EXPRESSION_KEY_PROP_EQUALS:
	{
		const char *prop;
		GValue *value;
		gboolean ret;

		ephy_history_node_get_property_by_id (node, 
						 exp->args.prop_args.prop_id,
						 &value);
		prop = g_value_get_string (value);
	
		if (prop == NULL)
			return FALSE;

		ret = (strcmp (prop, exp->args.prop_args.second_arg.string) == 0);
		g_value_unset (value);
		return ret;
	}
	case EPHY_HISTORY_FILTER_EXPRESSION_INT_PROP_EQUALS:
	{
		int prop;
		GValue *value;

		ephy_history_node_get_property_by_id (node, 
						 exp->args.prop_args.prop_id,
						 &value);
		prop = g_value_get_uint (value);
		g_value_unset (value);

		return (prop == exp->args.prop_args.second_arg.number);
	}
	case EPHY_HISTORY_FILTER_EXPRESSION_INT_PROP_BIGGER_THAN:
	{
		int prop;
		GValue *value;

		ephy_history_node_get_property_by_id (node, 
						 exp->args.prop_args.prop_id,
						 &value);
		prop = g_value_get_uint (value);
		g_value_unset (value);

		return (prop > exp->args.prop_args.second_arg.number);
	}
	case EPHY_HISTORY_FILTER_EXPRESSION_INT_PROP_LESS_THAN:
	{
		int prop;
		GValue *value;

		ephy_history_node_get_property_by_id (node, 
						 exp->args.prop_args.prop_id,
						 &value);
		prop = g_value_get_uint (value);
		g_value_unset (value);

		return (prop < exp->args.prop_args.second_arg.number);
	}
	default:
		break;
	}

	return FALSE;
}
