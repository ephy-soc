/* 
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 *  $Id: ephy-tree-model-node.c 6952 2007-03-11 19:42:02Z chpe $
 */

#include "config.h"

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <time.h>
#include <string.h>

#include "ephy-tree-model-history.h"
#include "ephy-stock-icons.h"
#include "ephy-debug.h"
#include "ephy-history.h"
#include "ephy-shell.h"

static void ephy_tree_model_history_class_init (EphyTreeModelHistoryClass *klass);
static void ephy_tree_model_history_init (EphyTreeModelHistory *model);
static void ephy_tree_model_history_finalize (GObject *object);
static void ephy_tree_model_history_tree_model_init (GtkTreeModelIface *iface);

#define EPHY_TREE_MODEL_HISTORY_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), EPHY_TYPE_TREE_MODEL_HISTORY, EphyTreeModelHistoryPrivate))

struct _EphyTreeModelHistoryPrivate
{
	EphyHistory *eh;
	GArray *nodes;
	gint node_type;

	GPtrArray *columns;
	int columns_num;

	int stamp;
};

typedef struct
{
	GType type;
	int prop_id;
	EphyTreeModelHistoryValueFunc func;
	gpointer user_data;
} EphyTreeModelHistoryColData;

enum
{
	PROP_0,
	PROP_NODES,
	PROP_NODE_TYPE
};

typedef struct
{
        guint64 id;
} HistoryNode;

static GObjectClass *parent_class = NULL;

GType
ephy_tree_model_history_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0))
	{
		const GTypeInfo our_info =
		{
			sizeof (EphyTreeModelHistoryClass),
			NULL,
			NULL,
			(GClassInitFunc) ephy_tree_model_history_class_init,
			NULL,
			NULL,
			sizeof (EphyTreeModelHistory),
			0,
			(GInstanceInitFunc) ephy_tree_model_history_init
		};

		const GInterfaceInfo tree_model_info =
		{
			(GInterfaceInitFunc) ephy_tree_model_history_tree_model_init,
			NULL,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT,
					       "EphyTreeModelHistory",
					       &our_info, 0);

		g_type_add_interface_static (type,
					     GTK_TYPE_TREE_MODEL,
					     &tree_model_info);
	}

	return type;
}

static inline void
iter_from_node (EphyTreeModelHistory *model,
	  	guint64 id,
		GtkTreeIter *iter)
{
        /* XXX: we need to free this some place? */
        HistoryNode *node = g_new (HistoryNode, 1);
        node->id = id;

	iter->stamp = model->priv->stamp;
	iter->user_data = node;
	iter->user_data2 = NULL;
}

static int
node_ids_bsearch_compare (const void *key, const void *memberp)
{
        return (*(guint64 *) key) - (*(guint64 *) memberp);
}

static int
get_node_index (GArray *nodes, guint64 id)
{
        int index;
        char *ptr;

        ptr = bsearch (&id, nodes->data, nodes->len, sizeof (guint64),
                       node_ids_bsearch_compare);
        if (ptr == NULL)
                return -1;

        index = (ptr - nodes->data) / sizeof (guint64);
        if (index >= nodes->len)
                return -1;

        return index;
}

static inline GtkTreePath *
get_path_real (EphyTreeModelHistory *model, 
	       guint64 id)
{
        GtkTreePath *retval;
        int index;

        index = get_node_index (model->priv->nodes, id);

        if (index == -1)
                return NULL;

        retval = gtk_tree_path_new ();
        gtk_tree_path_append_index (retval, index);

        return retval;
}

/* XXX: EphyHistory signal handling is not implemented yet */
#ifdef ENABLE_HISTORY_SIGNALS

static void
root_child_removed_cb (EphyNode *node,
		       EphyNode *child,
		       guint old_index,
		       EphyTreeModelHistory *model)
{
	GtkTreePath *path;

	path = gtk_tree_path_new ();
	gtk_tree_path_append_index (path, old_index);
	gtk_tree_model_row_deleted (GTK_TREE_MODEL (model), path);
	gtk_tree_path_free (path);
}

static void
root_child_added_cb (EphyNode *node,
		     EphyNode *child,
		     EphyTreeModelHistory *model)
{
	GtkTreePath *path;
	GtkTreeIter iter;

	ephy_tree_model_history_iter_from_node (model, child, &iter);

	path = get_path_real (model, child);
	gtk_tree_model_row_inserted (GTK_TREE_MODEL (model), path, &iter);
	gtk_tree_path_free (path);
}

static inline void
ephy_tree_model_history_update_node (EphyTreeModelHistory *model,
				  EphyNode *node,
				  int idx)
{
	GtkTreePath *path;
	GtkTreeIter iter;

	ephy_tree_model_history_iter_from_node (model, node, &iter);

	if (idx >= 0)
	{
		path = gtk_tree_path_new ();
		gtk_tree_path_append_index (path, idx);
	}
	else
	{
		path = get_path_real (model, node);
	}

	LOG ("Updating row");

	gtk_tree_model_row_changed (GTK_TREE_MODEL (model), path, &iter);
	gtk_tree_path_free (path);
}

static void
root_child_changed_cb (EphyNode *node,
		       EphyNode *child,
		       guint property_id,
		       EphyTreeModelHistory *model)
{
	ephy_tree_model_history_update_node (model, child, -1);
}

static void
root_children_reordered_cb (EphyNode *node,
			    int *new_order,
			    EphyTreeModelHistory *model)
{
	GtkTreePath *path;

	path = gtk_tree_path_new ();
	gtk_tree_model_rows_reordered (GTK_TREE_MODEL (model), path, NULL, new_order);
	gtk_tree_path_free (path);
}

static void
root_destroy_cb (EphyNode *node,
		 EphyTreeModelHistory *model)
{
	model->priv->nodes = NULL;

	/* no need to do other stuff since we should have had a bunch of child_removed
	 * signals already */
}
#endif

static void
ephy_tree_model_history_set_property (GObject *object,
			           guint prop_id,
			           const GValue *value,
			           GParamSpec *pspec)
{
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (object);

	switch (prop_id)
	{
	case PROP_NODES:
		model->priv->nodes = g_value_get_pointer (value);
		break;
	case PROP_NODE_TYPE:
		model->priv->node_type = g_value_get_int (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
ephy_tree_model_history_get_property (GObject *object,
			           guint prop_id,
				   GValue *value,
			           GParamSpec *pspec)
{
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (object);

	switch (prop_id)
	{
	case PROP_NODES:
		g_value_set_pointer (value, model->priv->nodes);
		break;
	case PROP_NODE_TYPE:
		g_value_set_int(value, model->priv->node_type);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
ephy_tree_model_history_class_init (EphyTreeModelHistoryClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = ephy_tree_model_history_finalize;

	object_class->set_property = ephy_tree_model_history_set_property;
	object_class->get_property = ephy_tree_model_history_get_property;

	g_object_class_install_property (object_class,
					 PROP_NODES,
					 g_param_spec_pointer ("nodes",
							      "History nodes",
							      "History nodes",
							      G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (object_class,
					 PROP_NODE_TYPE,
					 g_param_spec_int ("node-type",
							      "node type",
							      "node type",
							      G_MININT,
							      G_MAXINT,
							      0,	
							      G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_CONSTRUCT_ONLY));

	g_type_class_add_private (object_class, sizeof (EphyTreeModelHistoryPrivate));
}

static void
ephy_tree_model_history_init (EphyTreeModelHistory *model)
{
	model->priv = EPHY_TREE_MODEL_HISTORY_GET_PRIVATE (model);
	model->priv->stamp = g_random_int ();
	model->priv->columns = g_ptr_array_new ();
	model->priv->columns_num = 0;

	model->priv->eh = EPHY_HISTORY (ephy_embed_shell_get_global_history 
							(embed_shell));

	/* XXX: EphyHistory signal handling is not implemented yet */
#ifdef ENABLE_HISTORY_SIGNALS
	ephy_node_signal_connect_object (model->priv->eh,
					 EPHY_NODE_CHILD_ADDED,
					 (EphyNodeCallback) node_child_added_cb,
					 G_OBJECT (model));
	ephy_node_signal_connect_object (model->priv->eh,
					 EPHY_NODE_CHILD_REMOVED,
					 (EphyNodeCallback) node_child_removed_cb,
					 G_OBJECT (model));
	ephy_node_signal_connect_object (model->priv->eh,
					 EPHY_NODE_CHILD_CHANGED,
					 (EphyNodeCallback) node_child_changed_cb,
					 G_OBJECT (model));
	ephy_node_signal_connect_object (model->priv->eh,
					 EPHY_NODE_CHILDREN_REORDERED,
					 (EphyNodeCallback) node_children_reordered_cb,
					 G_OBJECT (model));
	ephy_node_signal_connect_object (model->priv->node,
					 EPHY_NODE_DESTROY,
					 (EphyNodeCallback) node_destroy_cb,
					 G_OBJECT (model));
#endif
}

static void
ephy_tree_model_history_finalize (GObject *object)
{
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (object);

	g_ptr_array_foreach (model->priv->columns, (GFunc) g_free, NULL);
	g_ptr_array_free (model->priv->columns, TRUE);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

EphyTreeModelHistory *
ephy_tree_model_history_new (GArray *nodes, gint node_type)
{
	EphyTreeModelHistory *model;

	model = EPHY_TREE_MODEL_HISTORY (g_object_new (
						EPHY_TYPE_TREE_MODEL_HISTORY,
						"nodes", nodes,
						"node-type", node_type,
						NULL));

	g_return_val_if_fail (model->priv != NULL, NULL);

	return model;
}

int
ephy_tree_model_history_add_prop_column (EphyTreeModelHistory *model,
				      GType value_type,
				      int prop_id)
{
	EphyTreeModelHistoryColData *col;
	int col_id;

	col = g_new0 (EphyTreeModelHistoryColData, 1);
	col->prop_id = prop_id;
	col->type = value_type;
	col->func = NULL;
	col->user_data = NULL;

	g_ptr_array_add (model->priv->columns, col);
	col_id = model->priv->columns_num;
	model->priv->columns_num++;

	return col_id;
}

int
ephy_tree_model_history_add_func_column (EphyTreeModelHistory *model,
				      GType value_type,
				      EphyTreeModelHistoryValueFunc func,
				      gpointer user_data)
{
	EphyTreeModelHistoryColData *col;
	int col_id;

	col = g_new0 (EphyTreeModelHistoryColData, 1);
	col->prop_id = -1;
	col->type = value_type;
	col->func = func;
	col->user_data = user_data;

	g_ptr_array_add (model->priv->columns, col);
	col_id = model->priv->columns_num;
	model->priv->columns_num++;

	return col_id;
}

static int
ephy_tree_model_history_get_n_columns (GtkTreeModel *tree_model)
{
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (tree_model);

	return model->priv->columns_num;
}

static GType
ephy_tree_model_history_get_column_type (GtkTreeModel *tree_model,
			              int index)
{
	EphyTreeModelHistoryColData *col;
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (tree_model);

	col = g_ptr_array_index (model->priv->columns, index);

	return col->type;
}

static void
ephy_tree_model_history_get_value (GtkTreeModel *tree_model,
			        GtkTreeIter *iter,
			        int column,
			        GValue *value)
{
	EphyTreeModelHistoryColData *col;
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (tree_model);
	EphyHistoryNode *node;
	guint64 id;

	g_return_if_fail (EPHY_IS_TREE_MODEL_HISTORY (tree_model));
	g_return_if_fail (iter != NULL);
	g_return_if_fail (iter->stamp == model->priv->stamp);

	if (model->priv->nodes == NULL)
		return;

	col = g_ptr_array_index (model->priv->columns, column);
	g_return_if_fail (col != NULL);

	id = ((HistoryNode *) iter->user_data)->id;
	
	/* get the full node info from the id depending on the node type */

	/* XXX: Use a better approach to cache the page info 
 	 * 	Not even sure this hack works!
 	 **/
	node = iter->user_data2;
	if (node == NULL)
	{
		g_debug ("querying node info"); 
		switch (model->priv->node_type) 
		{
			case EPHY_HISTORY_TYPE_NODE:
				break;
			case EPHY_HISTORY_TYPE_SITE_NODE:
				node = ephy_history_get_site_by_id (model->priv->eh, id);
				break;
			case EPHY_HISTORY_TYPE_PAGE_NODE:
				node = ephy_history_get_page_by_id (model->priv->eh, id);
				break;
			case EPHY_HISTORY_TYPE_VISIT_NODE:
				break;
			default:
				g_assert_not_reached();
				break;	
		}
		iter->user_data2 = node;
	}
	else 
	{
		g_debug ("using cached page info"); 
	}

        if (col->prop_id >= 0)
        {
		char *str = NULL;
		guint64 last_visit;

		/* XXX: add more props */
		switch (col->prop_id)
		{
			case EPHY_HISTORY_NODE_PROP_URL:
				g_object_get (G_OBJECT (node), "url", 
					      &str, NULL);
			    	g_value_set_string (value, str);
				g_free (str);
				break;
			case EPHY_HISTORY_NODE_PROP_TITLE:
				g_object_get (G_OBJECT (node), "title", 
					      &str, NULL);
			    	g_value_set_string (value, str);
				g_free (str);
				break;
			case EPHY_HISTORY_NODE_PROP_FAVICON_URL:
				g_object_get (G_OBJECT (node), "favicon-url", 
					      &str, NULL);
			    	g_value_set_string (value, str);
				g_free (str);
				break;
			case EPHY_HISTORY_NODE_PROP_LAST_VISIT:
				g_object_get (G_OBJECT (node), "last-visit", 
					      &last_visit, NULL);
			    	g_value_set_uint64 (value, last_visit);
				break;
			default:
				g_assert_not_reached();
				break;
		}	
                /* make sure to return a valid string anyway */
		if (str == NULL && col->type == G_TYPE_STRING)
		{
			g_value_init (value, col->type);
			g_value_set_string (value, "");
		}
	}
        else
        {
                col->func (node, value, col->user_data);
        }
}

static GtkTreeModelFlags
ephy_tree_model_history_get_flags (GtkTreeModel *tree_model)
{
	return GTK_TREE_MODEL_ITERS_PERSIST | GTK_TREE_MODEL_LIST_ONLY;
}

static gboolean
ephy_tree_model_history_get_iter (GtkTreeModel *tree_model,
			       GtkTreeIter *iter,
			       GtkTreePath *path)
{
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (tree_model);
	int index;
	guint64 id;

	g_return_val_if_fail (EPHY_IS_TREE_MODEL_HISTORY (model), FALSE);
	g_return_val_if_fail (gtk_tree_path_get_depth (path) > 0, FALSE);

	if (model->priv->nodes == NULL)
		return FALSE;

        index = gtk_tree_path_get_indices (path)[0];
        id = g_array_index (model->priv->nodes, guint64, index);
        iter_from_node (model, id, iter);

	return TRUE;
}


static GtkTreePath *
ephy_tree_model_history_get_path (GtkTreeModel *tree_model,
			       GtkTreeIter *iter)
{
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (tree_model);
        guint64 id;

	g_return_val_if_fail (EPHY_IS_TREE_MODEL_HISTORY (tree_model), NULL);
	g_return_val_if_fail (iter != NULL, NULL);
	g_return_val_if_fail (iter->user_data != NULL, NULL);
	g_return_val_if_fail (iter->stamp == model->priv->stamp, NULL);

	if (model->priv->nodes == NULL)
		return NULL;

        id = ((HistoryNode *) iter->user_data)->id;
        return get_path_real (model, id);
}

static gboolean
ephy_tree_model_history_iter_next (GtkTreeModel *tree_model,
			        GtkTreeIter *iter)
{
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (tree_model);
        guint64 id;
        gint index, next;

	g_return_val_if_fail (iter != NULL, FALSE);
	g_return_val_if_fail (iter->user_data != NULL, FALSE);
	g_return_val_if_fail (iter->stamp == model->priv->stamp, FALSE);

	if (model->priv->nodes == NULL)
		return FALSE;

        id = ((HistoryNode *) iter->user_data)->id;
        index = get_node_index (model->priv->nodes, id);
        if (index == -1)
                return FALSE;

        next = index + 1;
        if (next >= model->priv->nodes->len)
        {
        	return FALSE;
        }

        id = g_array_index (model->priv->nodes, guint64, next);

        iter_from_node (model, id, iter);

	return (iter->user_data != NULL);
}

static gboolean
ephy_tree_model_history_iter_children (GtkTreeModel *tree_model,
			            GtkTreeIter *iter,
			            GtkTreeIter *parent)
{
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (tree_model);
        guint64 id;

	if (model->priv->nodes == NULL)
		return FALSE;

	if (parent != NULL)
		return FALSE;

        if (model->priv->nodes->len > 0)
        {
                id = g_array_index (model->priv->nodes, guint64, 0);
        }
        else
        {
                return FALSE;
        }

        iter_from_node (model, id, iter);

	return TRUE;
}

static gboolean
ephy_tree_model_history_iter_has_child (GtkTreeModel *tree_model,
			             GtkTreeIter *iter)
{
	return FALSE;
}

static int
ephy_tree_model_history_iter_n_children (GtkTreeModel *tree_model,
			              GtkTreeIter *iter)
{
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (tree_model);

	g_return_val_if_fail (EPHY_IS_TREE_MODEL_HISTORY (tree_model), -1);

	if (model->priv->nodes == NULL)
		return 0;

	if (iter == NULL)
		return model->priv->nodes->len;

	g_return_val_if_fail (model->priv->stamp == iter->stamp, -1);

	return 0;
}

static gboolean
ephy_tree_model_history_iter_nth_child (GtkTreeModel *tree_model,
			             GtkTreeIter *iter,
			             GtkTreeIter *parent,
			             int n)
{
	EphyTreeModelHistory *model = EPHY_TREE_MODEL_HISTORY (tree_model);
	guint64 id;

	g_return_val_if_fail (EPHY_IS_TREE_MODEL_HISTORY (tree_model), FALSE);

	if (model->priv->nodes == NULL)
		return FALSE;

	if (parent != NULL)
		return FALSE;

        id = g_array_index (model->priv->nodes, guint64, n);
	iter_from_node (model, id, iter);

	return TRUE;
}

static gboolean
ephy_tree_model_history_iter_parent (GtkTreeModel *tree_model,
			          GtkTreeIter *iter,
			          GtkTreeIter *child)
{
	return FALSE;
}

/* XXX: re-factor with get_value() */
EphyHistoryNode *
ephy_tree_model_history_node_from_iter (EphyTreeModelHistory *model,
				     GtkTreeIter *iter)
{
	EphyHistoryNode *node; 
	guint64 id;

	node = iter->user_data2;
	id = ((HistoryNode *) iter->user_data)->id;
	if (node == NULL)
	{
		g_debug ("querying node info"); 
		switch (model->priv->node_type) 
		{
			case EPHY_HISTORY_TYPE_NODE:
				break;
			case EPHY_HISTORY_TYPE_SITE_NODE:
				node = ephy_history_get_site_by_id (model->priv->eh, id);
				break;
			case EPHY_HISTORY_TYPE_PAGE_NODE:
				node = ephy_history_get_page_by_id (model->priv->eh, id);
				break;
			case EPHY_HISTORY_TYPE_VISIT_NODE:
				break;
			default:
				g_assert_not_reached();
				break;	
		}
		iter->user_data2 = node;
	}
	return node;
}

void
ephy_tree_model_history_iter_from_node (EphyTreeModelHistory *model,
				     guint64 id,
				     GtkTreeIter *iter)
{
	iter_from_node(model, id, iter);
}

static void
ephy_tree_model_history_tree_model_init (GtkTreeModelIface *iface)
{
	iface->get_flags       = ephy_tree_model_history_get_flags;
	iface->get_iter        = ephy_tree_model_history_get_iter;
	iface->get_path        = ephy_tree_model_history_get_path;
	iface->iter_next       = ephy_tree_model_history_iter_next;
	iface->iter_children   = ephy_tree_model_history_iter_children;
	iface->iter_has_child  = ephy_tree_model_history_iter_has_child;
	iface->iter_n_children = ephy_tree_model_history_iter_n_children;
	iface->iter_nth_child  = ephy_tree_model_history_iter_nth_child;
	iface->iter_parent     = ephy_tree_model_history_iter_parent;
	iface->get_n_columns   = ephy_tree_model_history_get_n_columns;
	iface->get_column_type = ephy_tree_model_history_get_column_type;
	iface->get_value       = ephy_tree_model_history_get_value;
}
