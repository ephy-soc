/*
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 *  $Id: ephy-node-view.h 6952 2007-03-11 19:42:02Z chpe $
 */

#ifndef __EPHY_HISTORY_VIEW_H
#define __EPHY_HISTORY_VIEW_H

#include <gtk/gtktreeview.h>
#include <gtk/gtkdnd.h>

#include "ephy-tree-model-history.h"
#include "ephy-node-filter.h"

G_BEGIN_DECLS

#define EPHY_TYPE_HISTORY_VIEW         (ephy_history_view_get_type ())
#define EPHY_HISTORY_VIEW(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), EPHY_TYPE_HISTORY_VIEW, EphyHistoryView))
#define EPHY_HISTORY_VIEW_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), EPHY_TYPE_HISTORY_VIEW, EphyHistoryViewClass))
#define EPHY_IS_HISTORY_VIEW(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EPHY_TYPE_HISTORY_VIEW))
#define EPHY_IS_HISTORY_VIEW_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), EPHY_TYPE_HISTORY_VIEW))
#define EPHY_HISTORY_VIEW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), EPHY_TYPE_HISTORY_VIEW, EphyHistoryViewClass))

typedef struct _EphyHistoryViewPrivate EphyHistoryViewPrivate;

typedef struct
{
	GtkTreeView parent;

	/*< private >*/
	EphyHistoryViewPrivate *priv;
} EphyHistoryView;

typedef enum
{
	EPHY_HISTORY_VIEW_ALL_PRIORITY,
	EPHY_HISTORY_VIEW_SPECIAL_PRIORITY,
	EPHY_HISTORY_VIEW_NORMAL_PRIORITY
} EphyHistoryViewPriority;

typedef enum
{
	EPHY_HISTORY_VIEW_SHOW_PRIORITY = 1 << 0,
	EPHY_HISTORY_VIEW_SORTABLE = 1 << 1,
	EPHY_HISTORY_VIEW_EDITABLE = 1 << 2,
	EPHY_HISTORY_VIEW_SEARCHABLE = 1 << 3
} EphyHistoryViewFlags;

typedef struct
{
	GtkTreeViewClass parent;

	void (*node_toggled)	    (EphyHistoryView *view, EphyNode *node, gboolean checked);
	void (*node_activated)      (EphyHistoryView *view, EphyNode *node);
	void (*node_selected)       (EphyHistoryView *view, EphyNode *node);
	void (*node_dropped)        (EphyHistoryView *view, EphyNode *node, GList *uris);
} EphyHistoryViewClass;

GType      ephy_history_view_get_type	      (void);

GtkWidget *ephy_history_view_new                 (EphyNode *root,
					       EphyNodeFilter *filter);

void	   ephy_history_view_enable_dnd	      (EphyHistoryView *view);

void	   ephy_history_view_add_toggle	      (EphyHistoryView *view,
					       EphyTreeModelNodeValueFunc value_func,
					       gpointer data);

int	   ephy_history_view_add_column	      (EphyHistoryView *view,
					       const char  *title,
					       GType value_type,
					       guint prop_id,
					       EphyHistoryViewFlags flags,
					       EphyTreeModelNodeValueFunc icon_func,
					       GtkTreeViewColumn **column);

int	   ephy_history_view_add_data_column     (EphyHistoryView *view,
			                       GType value_type,
					       guint prop_id,
			                       EphyTreeModelNodeValueFunc func,
					       gpointer data);

void	   ephy_history_view_set_sort            (EphyHistoryView *view,
			                       GType value_type,
					       guint prop_id,
					       GtkSortType sort_type);

void	   ephy_history_view_set_priority        (EphyHistoryView *view,
					       guint priority_prop_id);

void	   ephy_history_view_remove              (EphyHistoryView *view);

GList     *ephy_history_view_get_selection       (EphyHistoryView *view);

void	   ephy_history_view_select_node         (EphyHistoryView *view,
			                       EphyNode *node);

void	   ephy_history_view_enable_drag_source  (EphyHistoryView *view,
					       const GtkTargetEntry *types,
					       int n_types,
					       int base_drag_column_id,
					       int extra_drag_column_id);

void	   ephy_history_view_enable_drag_dest    (EphyHistoryView *view,
					       const GtkTargetEntry *types,
					       int n_types);

void	   ephy_history_view_edit		      (EphyHistoryView *view,
					       gboolean remove_if_cancelled);

gboolean   ephy_history_view_is_target	      (EphyHistoryView *view);

void	   ephy_history_view_popup		      (EphyHistoryView *view,
					       GtkWidget *menu);

G_END_DECLS

#endif /* EPHY_HISTORY_VIEW_H */
