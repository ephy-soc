/*
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 *  $Id: ephy-tree-model-node.h 6952 2007-03-11 19:42:02Z chpe $
 */

#ifndef __EPHY_TREE_MODEL_HISTORY_H
#define __EPHY_TREE_MODEL_HISTORY_H

#include <gtk/gtktreemodel.h>

#include "ephy-history-node.h"

G_BEGIN_DECLS

#define EPHY_TYPE_TREE_MODEL_HISTORY         (ephy_tree_model_history_get_type ())
#define EPHY_TREE_MODEL_HISTORY(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), EPHY_TYPE_TREE_MODEL_HISTORY, EphyTreeModelHistory))
#define EPHY_TREE_MODEL_HISTORY_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), EPHY_TYPE_TREE_MODEL_HISTORY, EphyTreeModelHistoryClass))
#define EPHY_IS_TREE_MODEL_HISTORY(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EPHY_TYPE_TREE_MODEL_HISTORY))
#define EPHY_IS_TREE_MODEL_HISTORY_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), EPHY_TYPE_TREE_MODEL_HISTORY))
#define EPHY_TREE_MODEL_HISTORY_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), EPHY_TYPE_TREE_MODEL_HISTORY, EphyTreeModelHistoryClass))

typedef void (*EphyTreeModelHistoryValueFunc) (EphyHistoryNode *node, 
				GValue *value, gpointer user_data);

typedef struct _EphyTreeModelHistoryPrivate EphyTreeModelHistoryPrivate;

typedef struct
{
	GObject parent;

	/*< private >*/
	EphyTreeModelHistoryPrivate *priv;
} EphyTreeModelHistory;

typedef struct
{
	GObjectClass parent;
} EphyTreeModelHistoryClass;

GType              ephy_tree_model_history_get_type         (void);

EphyTreeModelHistory *ephy_tree_model_history_new	    (GArray *root, gint node_type);

int                ephy_tree_model_history_add_prop_column  (EphyTreeModelHistory *model,
						          GType value_type,
						          int prop_id);

int                ephy_tree_model_history_add_func_column  (EphyTreeModelHistory *model,
						          GType value_type,
						          EphyTreeModelHistoryValueFunc func,
						          gpointer user_data);

EphyHistoryNode          *ephy_tree_model_history_node_from_iter   (EphyTreeModelHistory *model,
						          GtkTreeIter *iter);

void               ephy_tree_model_history_iter_from_node   (EphyTreeModelHistory *model,
						          guint64 id,
						          GtkTreeIter *iter);

G_END_DECLS

#endif /* EPHY_TREE_MODEL_HISTORY_H */
