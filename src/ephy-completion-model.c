/*
 * -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 */
/*
 *  Copyright © 2002 Jorn Baayen <jorn@nl.linux.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 *  $Id: ephy-completion-model.c 7020 2007-05-01 17:13:10Z xan $
 */

#include "config.h"

#include "ephy-completion-model.h"
#include "ephy-favicon-cache.h"
#include "ephy-node.h"
#include "ephy-shell.h"
#include "ephy-history.h"

//#include <stdlib.h>

static void ephy_completion_model_class_init (EphyCompletionModelClass *
					      klass);
static void ephy_completion_model_init (EphyCompletionModel * model);
static void ephy_completion_model_tree_model_init (GtkTreeModelIface * iface);
static void ephy_completion_model_finalize (GObject *object);

#define EPHY_COMPLETION_MODEL_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), EPHY_TYPE_COMPLETION_MODEL, EphyCompletionModelPrivate))

struct _EphyCompletionModelPrivate
{
	EphyHistory *eh;
	GArray *history;
	GArray *bookmarks;
	int stamp;
};

enum
{
	HISTORY_GROUP,
	BOOKMARKS_GROUP
};

typedef struct
{
	guint64 id;
} HistoryItem;

static GObjectClass *parent_class = NULL;

GType
ephy_completion_model_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0))
	{
		const GTypeInfo our_info = {
			sizeof (EphyCompletionModelClass),
			NULL,
			NULL,
			(GClassInitFunc) ephy_completion_model_class_init,
			NULL,
			NULL,
			sizeof (EphyCompletionModel),
			0,
			(GInstanceInitFunc) ephy_completion_model_init
		};

		const GInterfaceInfo tree_model_info = {
			(GInterfaceInitFunc)
				ephy_completion_model_tree_model_init,
			NULL,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT,
					       "EphyCompletionModel",
					       &our_info, 0);

		g_type_add_interface_static (type,
					     GTK_TYPE_TREE_MODEL,
					     &tree_model_info);
	}

	return type;
}

static void
ephy_completion_model_class_init (EphyCompletionModelClass * klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = ephy_completion_model_finalize;

	parent_class = g_type_class_peek_parent (klass);

	g_type_class_add_private (object_class,
				  sizeof (EphyCompletionModelPrivate));
}

static void 
ephy_completion_model_finalize (GObject *object)
{
	EphyCompletionModel *model = EPHY_COMPLETION_MODEL (object);

	if (model->priv->history)
		g_array_free (model->priv->history, TRUE);

	if (model->priv->bookmarks)
		g_array_free (model->priv->bookmarks, TRUE);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static int
page_ids_bsearch_compare (const void *key, const void *memberp)
{
	return (*(guint64 *) key) - (*(guint64 *) memberp);
}

static int
get_page_index (GArray * pages, guint64 id)
{
	int index;
	char *ptr;

	ptr = bsearch (&id, pages->data, pages->len, sizeof (guint64),
		       page_ids_bsearch_compare);
	if (ptr == NULL)
		return -1;

	index = (ptr - pages->data) / sizeof (guint64);
	if (index >= pages->len)
		return -1;

	return index;
}

static GtkTreePath *
get_path_real (EphyCompletionModel * model, GArray * pages, guint64 id)
{
	GtkTreePath *retval;
	int index;

	index = get_page_index (pages, id);

	if (index == -1)
		return NULL;

	if (pages == model->priv->bookmarks)
	{
		index += model->priv->history->len;
	}

	retval = gtk_tree_path_new ();
	gtk_tree_path_append_index (retval, index);

	return retval;
}

static void
iter_from_page (EphyCompletionModel * model,
		GArray * pages, guint64 id, GtkTreeIter * iter)
{
	/* XXX: we need to free this some place? */
	HistoryItem *item = g_new (HistoryItem, 1);
	item->id = id;

	iter->stamp = model->priv->stamp;
	iter->user_data = item;
	iter->user_data2 = pages;
	iter->user_data3 = NULL;
}

static GArray *
get_pages (EphyCompletionModel * model, int *index)
{
	if (*index >= model->priv->history->len)
	{
		*index = *index - model->priv->history->len;

		if (*index < model->priv->bookmarks->len)
		{
			return model->priv->bookmarks;
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		return model->priv->history;
	}
}

static void
history_page_added_cb (EphyHistory * history,
		       guint64 id, EphyCompletionModel * model)
{
	GtkTreePath *path;
	GtkTreeIter iter;

	iter_from_page (model, model->priv->history, id, &iter);
	g_array_append_val (model->priv->history, id);

	path = gtk_tree_path_new ();
	gtk_tree_path_append_index (path, model->priv->history->len - 1);
	gtk_tree_model_row_inserted (GTK_TREE_MODEL (model), path, &iter);
	gtk_tree_path_free (path);
}

static void
history_page_removed_cb (EphyHistory * history,
			 guint64 id, EphyCompletionModel * model)
{
	GtkTreePath *path;
	guint index;
	
	index = get_page_index (model->priv->history, id);
	g_array_remove_index (model->priv->history, index);

	path = gtk_tree_path_new ();
	gtk_tree_path_append_index (path, index);
	gtk_tree_model_row_deleted (GTK_TREE_MODEL (model), path);
	gtk_tree_path_free (path);
}

static void
history_page_updated_cb (EphyHistory * history,
			 guint64 id, EphyCompletionModel * model)
{
	GtkTreePath *path;
	GtkTreeIter iter;

	iter_from_page (model, model->priv->history, id, &iter);

	path = get_path_real (model, model->priv->history, id);
	gtk_tree_model_row_changed (GTK_TREE_MODEL (model), path, &iter);
	gtk_tree_path_free (path);
}

static void
connect_signals (EphyCompletionModel * model, EphyHistory * history)
{
	g_signal_connect (history, "page-added",
			  G_CALLBACK (history_page_added_cb),
			  G_OBJECT (model));
	g_signal_connect (history, "page-removed",
			  G_CALLBACK (history_page_removed_cb),
			  G_OBJECT (model));
	g_signal_connect (history, "page-updated",
			  G_CALLBACK (history_page_updated_cb),
			  G_OBJECT (model));
}

static void
ephy_completion_model_init (EphyCompletionModel * model)
{
	EphyHistory *history;

	model->priv = EPHY_COMPLETION_MODEL_GET_PRIVATE (model);
	model->priv->stamp = g_random_int ();

	history = EPHY_HISTORY (ephy_embed_shell_get_global_history 
				(embed_shell));
	model->priv->history = ephy_history_get_page_ids (history);
	model->priv->eh = history;
	connect_signals (model, model->priv->eh);

	/*
	 * XXX: Bookmarks 
	 */
	model->priv->bookmarks = g_array_new (FALSE, FALSE, sizeof (guint64));
}



EphyCompletionModel *
ephy_completion_model_new (void)
{
	EphyCompletionModel *model;

	model = EPHY_COMPLETION_MODEL (g_object_new
				       (EPHY_TYPE_COMPLETION_MODEL, NULL));

	g_return_val_if_fail (model->priv != NULL, NULL);

	return model;
}

static int
ephy_completion_model_get_n_columns (GtkTreeModel * tree_model)
{
	return N_COL;
}

static GType
ephy_completion_model_get_column_type (GtkTreeModel * tree_model, int index)
{
	GType type = 0;

	switch (index)
	{
	case EPHY_COMPLETION_TEXT_COL:
	case EPHY_COMPLETION_ACTION_COL:
	case EPHY_COMPLETION_KEYWORDS_COL:
	case EPHY_COMPLETION_EXTRA_COL:
		type = G_TYPE_STRING;
		break;
	case EPHY_COMPLETION_FAVICON_COL:
		type = GDK_TYPE_PIXBUF;
		break;
	case EPHY_COMPLETION_RELEVANCE_COL:
		type = G_TYPE_INT;
		break;
	}

	return type;
}

static void
init_text_col (GValue * value, EphyHistoryPageNode * page, int group)
{
	const char *text;

	switch (group)
	{
	case BOOKMARKS_GROUP:
		text = "";
		break;
	case HISTORY_GROUP:
		g_object_get (G_OBJECT (page), "url", &text, NULL);
		break;
	default:
		text = "";
	}

	g_value_set_string (value, text);
}

static void
init_action_col (GValue * value, EphyHistoryPageNode * page, int group)
{
	const char *text;

	switch (group)
	{
	case BOOKMARKS_GROUP:
		text = "";
		break;
	case HISTORY_GROUP:
		g_object_get (G_OBJECT (page), "url", &text, NULL);
		break;
	default:
		text = "";
	}

	g_value_set_string (value, text);
}

static void
init_keywords_col (GValue * value, EphyHistoryPageNode * node, int group)
{
	const char *text = NULL;

	switch (group)
	{
	case BOOKMARKS_GROUP:
		break;
	}

	if (text == NULL)
	{
		text = "";
	}

	g_value_set_string (value, text);
}

static void
init_favicon_col (GValue * value, EphyHistoryPageNode * page, int group)
{
	const char *icon_location;
	EphyFaviconCache *cache;
	GdkPixbuf *pixbuf = NULL;

	cache = EPHY_FAVICON_CACHE
		(ephy_embed_shell_get_favicon_cache
		 (EPHY_EMBED_SHELL (ephy_shell)));

	switch (group)
	{
	case BOOKMARKS_GROUP:
		break;
	case HISTORY_GROUP:
		g_object_get (G_OBJECT (page), "favicon-url",
			      &icon_location, NULL);
		break;
	default:
		icon_location = NULL;
	}

	if (icon_location)
	{
		pixbuf = ephy_favicon_cache_get (cache, icon_location);
	}

	g_value_take_object (value, pixbuf);
}

static gboolean
is_base_address (const char *address)
{
	int slashes = 0;

	if (address == NULL)
		return FALSE;

	while (*address != '\0')
	{
		if (*address == '/')
			slashes++;

		address++;

		/*
		 * Base uris has 3 slashes like http://www.gnome.org/ 
		 */
		if (slashes == 3)
		{
			return (*address == '\0');
		}
	}

	return FALSE;
}

static void
init_relevance_col (GValue * value, EphyHistoryPageNode * page, int group)
{
	int relevance = 0;

	/*
	 * We have three ordered groups: history's base addresses,
	 * bookmarks, deep history addresses 
	 */

	if (group == BOOKMARKS_GROUP)
	{
		relevance = 1 << 5;
	}
	else if (group == HISTORY_GROUP)
	{
		char *url;
		guint visits;

		g_object_get (G_OBJECT (page), "url", &url, "visit-count",
			      &visits, NULL);

		visits = MIN (visits, (1 << 5) - 1);

		if (is_base_address (url))
		{
			relevance = visits << 10;
		}
		else
		{
			relevance = visits;
		}
		//g_print("url: %s visits: %u rel: %d\n", url, visits, relevance);
	}
	g_value_set_int (value, relevance);
}

static void
init_url_col (GValue * value, EphyHistoryPageNode * page, int group)
{
	const char *url = NULL;

	if (group == BOOKMARKS_GROUP)
	{
		url = "";
	}
	else if (group == HISTORY_GROUP)
	{
		g_object_get (G_OBJECT (page), "url", &url, NULL);
	}
	else
	{
		url = "";
	}
	g_value_set_string (value, url);
}

static void
ephy_completion_model_get_value (GtkTreeModel * tree_model,
				 GtkTreeIter * iter,
				 int column, GValue * value)
{
	int group;
	EphyCompletionModel *model = EPHY_COMPLETION_MODEL (tree_model);
	EphyHistoryPageNode *page;
	guint64 id;

	g_return_if_fail (EPHY_IS_COMPLETION_MODEL (tree_model));
	g_return_if_fail (iter != NULL);
	g_return_if_fail (iter->stamp == model->priv->stamp);

	id = ((HistoryItem *) iter->user_data)->id;
	group = (iter->user_data2 == model->priv->history) ?
		HISTORY_GROUP : BOOKMARKS_GROUP;

	/* XXX: use a better approach to cache the page info */
	page = iter->user_data3;
	if (page == NULL)
	{
		/* g_debug ("querying page info"); */
		page = ephy_history_get_page_by_id (model->priv->eh, id);
		iter->user_data3 = page;
	}
	else 
	{
		/* g_debug ("using cached page info"); */
	}

	switch (column)
	{
	case EPHY_COMPLETION_EXTRA_COL:
		g_value_init (value, G_TYPE_STRING);
		/*
		 * We set an additional text for the item title only for
		 * history, since we assume that people know the url of
		 * their bookmarks 
		 */
		if (group == HISTORY_GROUP)
		{
			char *title;
			g_object_get (G_OBJECT (page), "title", &title, NULL);
			g_value_set_string (value, title);
		}
		break;
	case EPHY_COMPLETION_TEXT_COL:
		g_value_init (value, G_TYPE_STRING);
		init_text_col (value, page, group);
		break;
	case EPHY_COMPLETION_FAVICON_COL:
		g_value_init (value, GDK_TYPE_PIXBUF);
		init_favicon_col (value, page, group);
		break;
	case EPHY_COMPLETION_ACTION_COL:
		g_value_init (value, G_TYPE_STRING);
		init_action_col (value, page, group);
		break;
	case EPHY_COMPLETION_KEYWORDS_COL:
		g_value_init (value, G_TYPE_STRING);
		init_keywords_col (value, page, group);
		break;
	case EPHY_COMPLETION_RELEVANCE_COL:
		g_value_init (value, G_TYPE_INT);
		init_relevance_col (value, page, group);
		break;
	case EPHY_COMPLETION_URL_COL:
		g_value_init (value, G_TYPE_STRING);
		init_url_col (value, page, group);
		break;
	}
}

static GtkTreeModelFlags
ephy_completion_model_get_flags (GtkTreeModel * tree_model)
{
	return GTK_TREE_MODEL_ITERS_PERSIST | GTK_TREE_MODEL_LIST_ONLY;
}

static gboolean
ephy_completion_model_get_iter (GtkTreeModel * tree_model,
				GtkTreeIter * iter, GtkTreePath * path)
{
	EphyCompletionModel *model = EPHY_COMPLETION_MODEL (tree_model);
	GArray *pages;
	int index;
	guint64 id;

	g_return_val_if_fail (EPHY_IS_COMPLETION_MODEL (model), FALSE);
	g_return_val_if_fail (gtk_tree_path_get_depth (path) == 1, FALSE);

	index = gtk_tree_path_get_indices (path)[0];

	pages = get_pages (model, &index);
	if (pages == NULL)
		return FALSE;

	id = g_array_index (pages, guint64, index);
	iter_from_page (model, pages, id, iter);

	return TRUE;
}

static GtkTreePath *
ephy_completion_model_get_path (GtkTreeModel * tree_model, GtkTreeIter * iter)
{
	EphyCompletionModel *model = EPHY_COMPLETION_MODEL (tree_model);
	guint64 id;

	g_return_val_if_fail (iter != NULL, NULL);
	g_return_val_if_fail (iter->user_data != NULL, NULL);
	g_return_val_if_fail (iter->user_data2 != NULL, NULL);
	g_return_val_if_fail (iter->stamp == model->priv->stamp, NULL);

	id = ((HistoryItem *) iter->user_data)->id;
	return get_path_real (model, iter->user_data2, id);
}

static gboolean
ephy_completion_model_iter_next (GtkTreeModel * tree_model,
				 GtkTreeIter * iter)
{
	EphyCompletionModel *model = EPHY_COMPLETION_MODEL (tree_model);
	GArray *pages;
	guint64 id;
	gint index, next;

	g_return_val_if_fail (iter != NULL, FALSE);
	g_return_val_if_fail (iter->user_data != NULL, FALSE);
	g_return_val_if_fail (iter->user_data2 != NULL, FALSE);
	g_return_val_if_fail (iter->stamp == model->priv->stamp, FALSE);

	id = ((HistoryItem *) iter->user_data)->id;
	pages = (GArray *) iter->user_data2;

	index = get_page_index (pages, id);
	if (index == -1)
		return FALSE;

	next = index + 1;
	if (next >= pages->len)
	{
		if (pages == model->priv->history
		    && model->priv->bookmarks->len > 0)
		{
			pages = model->priv->bookmarks;
			next = 0;
		}
		else
		{
			return FALSE;
		}
	}

	id = g_array_index (pages, guint64, next);

	iter_from_page (model, pages, id, iter);

	return TRUE;
}

static gboolean
ephy_completion_model_iter_children (GtkTreeModel * tree_model,
				     GtkTreeIter * iter, GtkTreeIter * parent)
{
	EphyCompletionModel *model = EPHY_COMPLETION_MODEL (tree_model);
	GArray *pages;
	guint64 id;

	if (parent != NULL)
	{
		return FALSE;
	}

	if (model->priv->history->len > 0)
	{
		pages = model->priv->history;
		id = g_array_index (pages, guint64, 0);
	}
	else if (model->priv->bookmarks->len > 0)
	{
		pages = model->priv->bookmarks;
		id = g_array_index (pages, guint64, 0);
	}
	else
	{
		return FALSE;
	}

	iter_from_page (model, pages, id, iter);

	return TRUE;
}

static gboolean
ephy_completion_model_iter_has_child (GtkTreeModel * tree_model,
				      GtkTreeIter * iter)
{
	return FALSE;
}

static int
ephy_completion_model_iter_n_children (GtkTreeModel * tree_model,
				       GtkTreeIter * iter)
{
	EphyCompletionModel *model = EPHY_COMPLETION_MODEL (tree_model);

	g_return_val_if_fail (EPHY_IS_COMPLETION_MODEL (tree_model), -1);

	if (iter == NULL)
	{
		return model->priv->history->len +
			model->priv->bookmarks->len;
	}

	g_return_val_if_fail (model->priv->stamp == iter->stamp, -1);

	return 0;
}

static gboolean
ephy_completion_model_iter_nth_child (GtkTreeModel * tree_model,
				      GtkTreeIter * iter,
				      GtkTreeIter * parent, int n)
{
	EphyCompletionModel *model = EPHY_COMPLETION_MODEL (tree_model);
	GArray *pages;
	guint64 id;

	g_return_val_if_fail (EPHY_IS_COMPLETION_MODEL (tree_model), FALSE);

	if (parent != NULL)
	{
		return FALSE;
	}

	pages = get_pages (model, &n);
	if (pages == NULL)
		return FALSE;

	id = g_array_index (pages, guint64, n);
	iter_from_page (model, pages, id, iter);

	return TRUE;
}

static gboolean
ephy_completion_model_iter_parent (GtkTreeModel * tree_model,
				   GtkTreeIter * iter, GtkTreeIter * child)
{
	return FALSE;
}

static void
ephy_completion_model_tree_model_init (GtkTreeModelIface * iface)
{
	iface->get_flags = ephy_completion_model_get_flags;
	iface->get_iter = ephy_completion_model_get_iter;
	iface->get_path = ephy_completion_model_get_path;
	iface->iter_next = ephy_completion_model_iter_next;
	iface->iter_children = ephy_completion_model_iter_children;
	iface->iter_has_child = ephy_completion_model_iter_has_child;
	iface->iter_n_children = ephy_completion_model_iter_n_children;
	iface->iter_nth_child = ephy_completion_model_iter_nth_child;
	iface->iter_parent = ephy_completion_model_iter_parent;
	iface->get_n_columns = ephy_completion_model_get_n_columns;
	iface->get_column_type = ephy_completion_model_get_column_type;
	iface->get_value = ephy_completion_model_get_value;
}
