/*
 *  Copyright © 2001 Philip Langdale
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 *  $Id: MozRegisterComponents.h 6952 2007-03-11 19:42:02Z chpe $
 */

#ifndef MOZREGISTERCOMPONENTS_H
#define MOZREGISTERCOMPONENTS_H

#include <glib.h>

gboolean mozilla_register_components		(void);

#endif /* MOZREGISTERCOMPONENTS_H */
