#ifndef EPHY_HISTORY_VISIT_NODE_H
#define EPHY_HISTORY_VISIT_NODE_H

#include <glib-object.h>
#include "ephy-history-node.h"

G_BEGIN_DECLS

#define EPHY_TYPE_HISTORY_VISIT_NODE          (ephy_history_visit_node_get_type ())

#define EPHY_HISTORY_VISIT_NODE(o)            (G_TYPE_CHECK_INSTANCE_CAST ((o), EPHY_TYPE_HISTORY_VISIT_NODE, EphyHistoryVisitNode))

#define EPHY_HISTORY_VISIT_NODE_CLASS(k)      (G_TYPE_CHECK_CLASS_CAST((k), EPHY_TYPE_HISTORY_VISIT_NODE, EphyHistoryVisitNodeClass))

#define EPHY_IS_HISTORY_VISIT_NODE(o)         (G_TYPE_CHECK_INSTANCE_TYPE ((o), EPHY_TYPE_HISTORY_VISIT_NODE))

#define EPHY_IS_HISTORY_VISIT_NODE_CLASS(k)   (G_TYPE_CHECK_CLASS_TYPE ((k), EPHY_TYPE_HISTORY_VISIT_NODE))

#define EPHY_HISTORY_VISIT_NODE_GET_CLASS(o)  (G_TYPE_INSTANCE_GET_CLASS ((o), EPHY_TYPE_HISTORY_VISIT_NODE, EphyHistoryVisitNodeClass))

typedef struct _EphyHistoryVisitNodeClass    EphyHistoryVisitNodeClass;
typedef struct _EphyHistoryVisitNode         EphyHistoryVisitNode;
typedef struct _EphyHistoryVisitNodePrivate  EphyHistoryVisitNodePrivate;

struct _EphyHistoryVisitNode
{
    GObject parent;

    /*< private >*/
    EphyHistoryVisitNodePrivate *priv;
};

struct _EphyHistoryVisitNodeClass
{
    GObjectClass parent_class;
};

G_END_DECLS

#endif /* EPHY_HISTORY_VISIT_NODE_H */
