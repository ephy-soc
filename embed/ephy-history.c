#include <sqlite3.h>
#include <string.h>
#include <libgnomevfs/gnome-vfs-uri.h>

#include "ephy-marshal.h"
#include "ephy-history.h"

G_DEFINE_TYPE (EphyHistory, ephy_history, G_TYPE_OBJECT)
#define EPHY_HISTORY_GET_PRIVATE(object)    (G_TYPE_INSTANCE_GET_PRIVATE ((object), EPHY_TYPE_HISTORY, EphyHistoryPrivate))
#define EPHY_DB_PATH "/tmp/ephy-history.sqlite"
#define ephy_db_return_on_error(db, rc, err)  do {      \
    if (rc != SQLITE_OK)                                \
    {                                                   \
        g_set_error (err,                               \
                     EPHY_DB_ERROR_QUARK,               \
                     rc,                                \
                     sqlite3_errmsg(db));               \
        return FALSE;                                   \
    }                                                   \
} while(0)

struct _EphyHistoryPrivate
{
 sqlite3 *db;
 gboolean enabled;

 /* pre-compiled queries */
 sqlite3_stmt *q_get_hosts;
 sqlite3_stmt *q_get_pages;
 sqlite3_stmt *q_get_page_by_id;
 sqlite3_stmt *q_get_site_by_id;

 sqlite3_stmt *q_get_page_info;
 sqlite3_stmt *q_get_host_info;
 sqlite3_stmt *q_get_favicon;
 sqlite3_stmt *q_get_page_visit_info;
 sqlite3_stmt *q_get_page_icon;
 sqlite3_stmt *q_get_page_id;
 sqlite3_stmt *q_get_site_id;
 sqlite3_stmt *q_get_favicon_id;
 sqlite3_stmt *q_add_page;
 sqlite3_stmt *q_add_site;
 sqlite3_stmt *q_add_favicon;
 sqlite3_stmt *q_add_visit;
 sqlite3_stmt *q_update_page_visit_info;
 sqlite3_stmt *q_update_page_title;
 sqlite3_stmt *q_update_page_icon;

 sqlite3_stmt *q_remove_page;
 sqlite3_stmt *q_remove_page_visits;
};

enum
{
 PROP_0,
 PROP_ENABLED
};

enum
{
 PAGE_ADDED,
 PAGE_REMOVED,
 PAGE_UPDATED,
 PAGE_VISITED,
 PAGE_REDIRECT,   
 HISTORY_CLEARED,
 LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void ephy_history_finalize (GObject * object);

/* Private functions */
static gboolean db_init (EphyHistory * eh, GError ** err);
static gboolean db_init_queries (EphyHistory * eh, GError ** err);
static gboolean db_table_exists (sqlite3 * db, const char *tblname,
             gboolean * exists, GError ** err);
static gboolean db_close (EphyHistory * eh, GError ** err);

static gboolean get_favicon_id (EphyHistory * eh, const char *url,
            gint64 * favicon_id);
static gboolean add_favicon (EphyHistory * eh, const char *url,
             gint64 * favicon_id);
static gboolean update_page_icon (EphyHistory * eh, const char *url,
              gint icon_id);

static gboolean get_page_visit_info (EphyHistory * eh, const char *url,
                 gint64 * page_id, int *visit_count);
static gboolean update_page_visit_info (EphyHistory * eh, gint64 page_id,
                int visit_count);
static gboolean add_page (EphyHistory * eh, const char *url,
          const char *title, int visit_count,
          gint64 * page_id);
static gboolean add_site (EphyHistory * eh, const char *url,
          gint64 * site_id);
static gboolean add_visit (EphyHistory * eh, gint64 page_id,
           guint64 timestamp, const char *ref_url,
           int trans_type, int visit_duration,
           gint64 * visit_id);

static gboolean update_page_title (EphyHistory * eh, const char *url,
               const char *title);

/* utility functions */
static guint64 get_current_time ();
static char *get_hostname_from_url (const char *url);

GQuark ephy_db_error_quark;

static void
ephy_history_set_enabled (EphyHistory * history, gboolean enabled)
{
	history->priv->enabled = enabled;
}

static void
ephy_history_set_property (GObject * object,
			   guint prop_id,
			   const GValue * value, GParamSpec * pspec)
{
	EphyHistory *history = EPHY_HISTORY (object);

	switch (prop_id)
	{
	case PROP_ENABLED:
		ephy_history_set_enabled (history,
					  g_value_get_boolean (value));
		break;
	}
}

static void
ephy_history_get_property (GObject * object,
			   guint prop_id, GValue * value, GParamSpec * pspec)
{
	EphyHistory *history = EPHY_HISTORY (object);

	switch (prop_id)
	{
	case PROP_ENABLED:
		g_value_set_boolean (value, history->priv->enabled);
		break;
	}
}

static void
ephy_history_class_init (EphyHistoryClass * klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = ephy_history_finalize;

	object_class->get_property = ephy_history_get_property;
	object_class->set_property = ephy_history_set_property;

	g_object_class_install_property (object_class,
					 PROP_ENABLED,
					 g_param_spec_boolean ("enabled",
							       "Enabled",
							       "Enabled",
							       TRUE,
							       G_PARAM_READWRITE
							       |
							       G_PARAM_STATIC_NAME
							       |
							       G_PARAM_CONSTRUCT
							       |
							       G_PARAM_STATIC_NICK
							       |
							       G_PARAM_STATIC_BLURB));

	signals[PAGE_ADDED] =
		g_signal_new ("page-added",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (EphyHistoryClass, page_added),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__UINT64,
			      G_TYPE_NONE, 1, G_TYPE_UINT64);
	signals[PAGE_REMOVED] =
		g_signal_new ("page-removed",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (EphyHistoryClass,
					       page_removed), NULL, NULL,
			      g_cclosure_marshal_VOID__UINT64, G_TYPE_NONE, 1,
			      G_TYPE_UINT64);
	signals[PAGE_UPDATED] =
		g_signal_new ("page-updated",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (EphyHistoryClass,
					       page_updated), NULL, NULL,
			      g_cclosure_marshal_VOID__UINT64, G_TYPE_NONE, 1,
			      G_TYPE_UINT64);
	signals[PAGE_VISITED] =
                g_signal_new ("page-visited",
                              G_OBJECT_CLASS_TYPE (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (EphyHistoryClass, page_visited),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__STRING,
                              G_TYPE_NONE,
                              1,
	            		      G_TYPE_STRING);
	signals[HISTORY_CLEARED] =
                g_signal_new ("history-cleared",
                              G_OBJECT_CLASS_TYPE (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (EphyHistoryClass, cleared),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__VOID,
                              G_TYPE_NONE,
                              0);
	signals[PAGE_REDIRECT] =
                g_signal_new ("page-redirect",
                              G_OBJECT_CLASS_TYPE (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (EphyHistoryClass, page_redirect),
                              NULL, NULL,
                              ephy_marshal_VOID__STRING_STRING,
                              G_TYPE_NONE,
                              2,
			                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE,
			                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE);

	g_type_class_add_private (object_class, sizeof (EphyHistoryPrivate));

	/* XXX: move this someplace better */
	ephy_db_error_quark = g_quark_from_static_string ("ephy-db-error");
}

static void
ephy_history_finalize (GObject * object)
{
	EphyHistory *eh = EPHY_HISTORY (object);
	db_close (eh, NULL);

	G_OBJECT_CLASS (ephy_history_parent_class)->finalize (object);
}

static void
ephy_history_init (EphyHistory * eh)
{
	eh->priv = EPHY_HISTORY_GET_PRIVATE (eh);

	db_init (eh, NULL);
	db_init_queries (eh, NULL);
}

/*
 * Private
 */

static gboolean
db_table_exists (sqlite3 * db, const char *tblname, gboolean * exists,
		 GError ** err)
{
	sqlite3_stmt *stmt;
	int rc;
	gchar *query;

	query = g_strdup_printf ("SELECT name FROM sqlite_master WHERE "
				 "type = 'table' AND name = '%s'", tblname);
	rc = sqlite3_prepare_v2 (db, query, -1, &stmt, NULL);
	g_free (query);
	ephy_db_return_on_error (db, rc, err);
	rc = sqlite3_step (stmt);
	if (rc == SQLITE_ROW)
	{
		*exists = TRUE;
	}
	else if (rc == SQLITE_DONE)
	{
		*exists = FALSE;
	}
	else if (rc == SQLITE_ERROR)
	{
		g_set_error (err,
			     EPHY_DB_ERROR_QUARK, rc, sqlite3_errmsg (db));
		return FALSE;
	}
	rc = sqlite3_finalize (stmt);
	return TRUE;
}

static gboolean
db_close (EphyHistory * eh, GError ** err)
{
	sqlite3 *db = eh->priv->db;
	int rc;

	rc = sqlite3_finalize (eh->priv->q_get_hosts);
	rc = sqlite3_finalize (eh->priv->q_get_pages);
	rc = sqlite3_finalize (eh->priv->q_get_page_info);
	rc = sqlite3_finalize (eh->priv->q_get_host_info);
	rc = sqlite3_finalize (eh->priv->q_get_favicon);
	rc = sqlite3_finalize (eh->priv->q_get_page_visit_info);
	rc = sqlite3_finalize (eh->priv->q_get_page_icon);
	rc = sqlite3_finalize (eh->priv->q_get_page_id);
	rc = sqlite3_finalize (eh->priv->q_get_site_id);
	rc = sqlite3_finalize (eh->priv->q_get_favicon_id);
	rc = sqlite3_finalize (eh->priv->q_add_page);
	rc = sqlite3_finalize (eh->priv->q_add_site);
	rc = sqlite3_finalize (eh->priv->q_add_favicon);
	rc = sqlite3_finalize (eh->priv->q_add_visit);
	rc = sqlite3_finalize (eh->priv->q_update_page_visit_info);
	rc = sqlite3_finalize (eh->priv->q_update_page_title);
	rc = sqlite3_finalize (eh->priv->q_update_page_icon);
	rc = sqlite3_finalize (eh->priv->q_remove_page);
	rc = sqlite3_finalize (eh->priv->q_remove_page_visits);
	rc = sqlite3_finalize (eh->priv->q_get_page_by_id);
	rc = sqlite3_finalize (eh->priv->q_get_site_by_id);

	rc = sqlite3_close (db);
	ephy_db_return_on_error (db, rc, err);

	return TRUE;
}

static gboolean
db_init (EphyHistory * eh, GError ** err)
{
	sqlite3 *db;
	int rc;
	gboolean tbl_exists;

	rc = sqlite3_open (EPHY_DB_PATH, &db);
	ephy_db_return_on_error (db, rc, err);
	eh->priv->db = db;

	/* create tables */
	if (db_table_exists (db, "ephy_pages", &tbl_exists, err) == FALSE)
	{
		return FALSE;
	}
	if (!tbl_exists)
	{
		//g_debug("No ephy_pages table");
		rc = sqlite3_exec (db,
				   "CREATE TABLE ephy_pages ("
				   "id INTEGER PRIMARY KEY,"
				   "url LONGVARCHAR,"
				   "title LONGVARCHAR,"
				   "site_id INTEGER,"
				   "favicon_id INTEGER,"
				   "visit_count INTEGER DEFAULT 0)",
				   NULL, NULL, NULL);
		ephy_db_return_on_error (db, rc, err);
	}
	else
	{
		//g_debug("ephy_pages table exists");
	}
	if (db_table_exists (db, "ephy_sites", &tbl_exists, err) == FALSE)
	{
		return FALSE;
	}
	if (!tbl_exists)
	{
		//g_debug("No ephy_sites table");
		rc = sqlite3_exec (db,
				   "CREATE TABLE ephy_sites ("
				   "id INTEGER PRIMARY KEY,"
				   "url LONGVARCHAR,"
				   "title LONGVARCHAR,"
				   "favicon_id INTEGER)", NULL, NULL, NULL);
		ephy_db_return_on_error (db, rc, err);
	}
	else
	{
		//g_debug("ephy_sites table exists");
	}
	if (db_table_exists (db, "ephy_favicons", &tbl_exists, err) == FALSE)
	{
		return FALSE;
	}
	if (!tbl_exists)
	{
		//g_debug("No ephy_favicons table");
		rc = sqlite3_exec (db,
				   "CREATE TABLE ephy_favicons ("
				   "id INTEGER PRIMARY KEY,"
				   "url LONGVARCHAR)", NULL, NULL, NULL);
		ephy_db_return_on_error (db, rc, err);
	}
	else
	{
		//g_debug("ephy_favicons table exists");
	}
	if (db_table_exists (db, "ephy_pagevisits", &tbl_exists, err) ==
	    FALSE)
	{
		return FALSE;
	}
	if (!tbl_exists)
	{
		//g_debug("No ephy_pagevisits table");
		rc = sqlite3_exec (db,
				   "CREATE TABLE ephy_pagevisits ("
				   "id INTEGER PRIMARY KEY,"
				   "page_id INTEGER,"
				   "visit_date INTEGER,"
				   "visit_ref INTEGER,"
				   "visit_type INTEGER,"
				   "visit_duration INTEGER)",
				   NULL, NULL, NULL);
		ephy_db_return_on_error (db, rc, err);
	}
	else
	{
		//g_debug("ephy_pagevisits table exists");
	}

	return TRUE;
}

static gboolean
db_init_queries (EphyHistory * eh, GError ** err)
{
	sqlite3 *db;
	int rc;

	db = eh->priv->db;
	/* All Sites */
	rc = sqlite3_prepare_v2 (db,
				 "SELECT s.id, s.url, s.title, COUNT(*), "
				 "MAX(visit_date), f.url FROM ephy_sites s JOIN "
				 "ephy_pages p ON p.site_id = s.id JOIN "
				 "ephy_pagevisits v ON p.id = v.page_id "
				 "LEFT OUTER JOIN ephy_favicons f ON "
				 "f.id = s.favicon_id GROUP BY s.id", -1,
				 &eh->priv->q_get_hosts, NULL);
	ephy_db_return_on_error (db, rc, err);

	/* All Pages */
	rc = sqlite3_prepare_v2 (db,
				 "SELECT p.id, p.url, p.title, p.site_id, p.visit_count, "
				 "(SELECT MAX(visit_date) AS last_visit FROM "
				 "ephy_pagevisits WHERE page_id = p.id), "
				 "f.url FROM ephy_pages p LEFT OUTER JOIN "
				 "ephy_favicons f ON p.favicon_id = f.id",
				 -1, &eh->priv->q_get_pages, NULL);
	ephy_db_return_on_error (db, rc, err);

	/* Page Info */
	rc = sqlite3_prepare_v2 (db,
				 "SELECT p.id, p.url, p.title, p.site_id, p.visit_count, "
				 "(SELECT MAX(visit_date) AS last_visit FROM "
				 "ephy_pagevisits WHERE page_id = p.id), "
				 "f.url FROM ephy_pages p LEFT OUTER JOIN "
				 "ephy_favicons f ON p.favicon_id = f.id "
				 "WHERE p.id = ?1", -1,
				 &eh->priv->q_get_page_by_id, NULL);
	ephy_db_return_on_error (db, rc, err);

	/* Site Info */
    /* XXX: compute last_visit and visit_count */
	rc = sqlite3_prepare_v2 (db,
				 "SELECT s.id, s.url, s.title, "
				 "f.url FROM ephy_sites s LEFT OUTER JOIN "
				 "ephy_favicons f ON s.favicon_id = f.id "
				 "WHERE p.id = ?1", -1,
				 &eh->priv->q_get_site_by_id, NULL);
	ephy_db_return_on_error (db, rc, err);

	/* Get Icon */
	rc = sqlite3_prepare_v2 (db,
				 "SELECT f.url FROM ephy_favicons f,ephy_pages p "
				 "WHERE p.url = ?1 AND p.favicon_id = f.id",
				 -1, &eh->priv->q_get_favicon, NULL);
	ephy_db_return_on_error (db, rc, err);

	/* Set Icon */
	rc = sqlite3_prepare_v2 (db, "SELECT id FROM ephy_favicons "
				 " WHERE url = ?1", -1,
				 &eh->priv->q_get_favicon_id, NULL);
	ephy_db_return_on_error (db, rc, err);

	rc = sqlite3_prepare_v2 (db, "INSERT INTO ephy_favicons "
				 "(url) VALUES (?1)", -1,
				 &eh->priv->q_add_favicon, NULL);
	ephy_db_return_on_error (db, rc, err);

	rc = sqlite3_prepare_v2 (db,
				 "UPDATE ephy_pages SET favicon_id = ?1 "
				 "WHERE url = ?2", -1,
				 &eh->priv->q_update_page_icon, NULL);
	ephy_db_return_on_error (db, rc, err);

	/* Add Page */
	rc = sqlite3_prepare_v2 (db,
				 "SELECT id, visit_count FROM ephy_pages "
				 "WHERE url = ?1", -1,
				 &eh->priv->q_get_page_visit_info, NULL);
	ephy_db_return_on_error (db, rc, err);

	rc = sqlite3_prepare_v2 (db,
				 "UPDATE ephy_pages SET visit_count = ?1 "
				 "WHERE id = ?2", -1,
				 &eh->priv->q_update_page_visit_info, NULL);
	ephy_db_return_on_error (db, rc, err);

	rc = sqlite3_prepare_v2 (db, "INSERT INTO ephy_pages "
				 "(url, title, visit_count, site_id) "
				 "VALUES (?1, ?2, ?3, ?4)", -1,
				 &eh->priv->q_add_page, NULL);
	ephy_db_return_on_error (db, rc, err);

	rc = sqlite3_prepare_v2 (db, "INSERT INTO ephy_sites "
				 "(url) VALUES (?1)", -1,
				 &eh->priv->q_add_site, NULL);
	ephy_db_return_on_error (db, rc, err);

	rc = sqlite3_prepare_v2 (db, "INSERT INTO ephy_pagevisits "
				 "(page_id, visit_date, visit_ref, "
				 "visit_type, visit_duration) "
				 "VALUES (?1, ?2, ?3, ?4, ?5)", -1,
				 &eh->priv->q_add_visit, NULL);
	ephy_db_return_on_error (db, rc, err);

	/* Misc */
	rc = sqlite3_prepare_v2 (db,
				 "SELECT url, title, visit_count, site_id, "
				 "favicon_id FROM ephy_pages WHERE url = ?1",
				 -1, &eh->priv->q_get_page_info, NULL);
	ephy_db_return_on_error (db, rc, err);
	rc = sqlite3_prepare_v2 (db, "SELECT url, favicon_id "
				 "FROM ephy_sites WHERE url = ?1", -1,
				 &eh->priv->q_get_host_info, NULL);
	ephy_db_return_on_error (db, rc, err);
	rc = sqlite3_prepare_v2 (db, "SELECT id FROM ephy_pages "
				 "WHERE url = ?1", -1,
				 &eh->priv->q_get_page_id, NULL);
	ephy_db_return_on_error (db, rc, err);
	rc = sqlite3_prepare_v2 (db, "SELECT id FROM ephy_sites "
				 "WHERE url = ?1", -1,
				 &eh->priv->q_get_site_id, NULL);
	ephy_db_return_on_error (db, rc, err);

	/* Update Page Title */
	rc = sqlite3_prepare_v2 (db, "UPDATE ephy_pages SET title = ?1 "
				 "WHERE url = ?2", -1,
				 &eh->priv->q_update_page_title, NULL);
	ephy_db_return_on_error (db, rc, err);

	/* Remove page */
	rc = sqlite3_prepare_v2 (db, "DELETE FROM ephy_pages WHERE url = ?1",
				 -1, &eh->priv->q_remove_page, NULL);
	ephy_db_return_on_error (db, rc, err);

	rc = sqlite3_prepare_v2 (db, "DELETE FROM ephy_pagevisits WHERE "
				 "page_id = ?1", -1, 
				 &eh->priv->q_remove_page_visits, NULL);
	ephy_db_return_on_error (db, rc, err);

	return TRUE;
}

static guint64
get_current_time ()
{
	guint64 time_now;
	GTimeVal tv;

	g_get_current_time (&tv);
	time_now = (guint64) tv.tv_sec * G_USEC_PER_SEC + tv.tv_usec;
	return time_now;
}

static char *
get_hostname_from_url (const char *url)
{
	GnomeVFSURI *vfs_uri = NULL;
	const char *host_name = NULL;
	const char *scheme = NULL;
	char *host_url = NULL;

	vfs_uri = gnome_vfs_uri_new (url);
	if (vfs_uri != NULL)
	{
		scheme = gnome_vfs_uri_get_scheme (vfs_uri);
		host_name = gnome_vfs_uri_get_host_name (vfs_uri);
		if (scheme == NULL)
			scheme = "Unknown";
		if (host_name != NULL)
			host_url =
				g_strconcat (scheme, "://", host_name, "/",
					     NULL);
	}

	return host_url;
};

static gboolean
get_page_visit_info (EphyHistory * eh, const char *url,
		     gint64 * page_id, int *visit_count)
{
	int rc;

	rc = sqlite3_bind_text (eh->priv->q_get_page_visit_info, 1, url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_step (eh->priv->q_get_page_visit_info);
	if (rc != SQLITE_ROW)
	{
		sqlite3_reset (eh->priv->q_get_page_visit_info);
		return FALSE;
	}
	*page_id = sqlite3_column_int64 (eh->priv->q_get_page_visit_info, 0);
	*visit_count =
		sqlite3_column_int (eh->priv->q_get_page_visit_info, 1);
	rc = sqlite3_reset (eh->priv->q_get_page_visit_info);
	ephy_db_return_on_error (eh->priv->db, rc, NULL);
	return TRUE;
}

static gboolean
get_page_id (EphyHistory * eh, const char *url, guint64 * page_id)
{
	int rc;

	rc = sqlite3_bind_text (eh->priv->q_get_page_id, 1, url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_step (eh->priv->q_get_page_id);
	if (rc != SQLITE_ROW)
	{
		sqlite3_reset (eh->priv->q_get_page_id);
		return FALSE;
	}
	*page_id = sqlite3_column_int64 (eh->priv->q_get_page_id, 0);
	sqlite3_reset (eh->priv->q_get_page_id);
	return TRUE;
}

static gboolean
get_site_id (EphyHistory * eh, const char *url, gint64 * site_id)
{
	int rc;

	rc = sqlite3_bind_text (eh->priv->q_get_site_id, 1, url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_step (eh->priv->q_get_site_id);
	if (rc != SQLITE_ROW)
	{
		sqlite3_reset (eh->priv->q_get_site_id);
		return FALSE;
	}
	*site_id = sqlite3_column_int64 (eh->priv->q_get_site_id, 0);
	sqlite3_reset (eh->priv->q_get_site_id);
	return TRUE;
}

static gboolean
get_favicon_id (EphyHistory * eh, const char *url, gint64 * favicon_id)
{
	int rc;

	rc = sqlite3_bind_text (eh->priv->q_get_favicon_id, 1, url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_step (eh->priv->q_get_favicon_id);
	if (rc != SQLITE_ROW)
	{
		sqlite3_reset (eh->priv->q_get_favicon_id);
		return FALSE;
	}
	*favicon_id = sqlite3_column_int64 (eh->priv->q_get_favicon_id, 0);
	sqlite3_reset (eh->priv->q_get_favicon_id);
	return TRUE;
}

static gboolean
update_page_title (EphyHistory * eh, const char *url, const char *title)
{
	guint64 id;
	int rc;

	/* XXX: update only if title changed */
	rc = sqlite3_bind_text (eh->priv->q_update_page_title, 1, title, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_bind_text (eh->priv->q_update_page_title, 2, url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_step (eh->priv->q_update_page_title);
	sqlite3_reset (eh->priv->q_update_page_title);
	if (rc != SQLITE_DONE)
		return FALSE;

	/* XXX: emit url instead? */
	rc = get_page_id (eh, url, &id);
	g_signal_emit (G_OBJECT (eh), signals[PAGE_UPDATED], 0, id);
	return TRUE;
}

static gboolean
update_page_icon (EphyHistory * eh, const char *url, gint icon_id)
{
	int rc;

	/* XXX: update only if icon changed */
	rc = sqlite3_bind_int64 (eh->priv->q_update_page_icon, 1, icon_id);
	rc = sqlite3_bind_text (eh->priv->q_update_page_icon, 2, url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_step (eh->priv->q_update_page_icon);
	sqlite3_reset (eh->priv->q_update_page_icon);
	if (rc != SQLITE_DONE)
		return FALSE;
	return TRUE;
}

static gboolean
update_page_visit_info (EphyHistory * eh, gint64 page_id, int visit_count)
{
	int rc;

	rc = sqlite3_bind_int (eh->priv->q_update_page_visit_info, 1,
			       visit_count);
	rc = sqlite3_bind_int64 (eh->priv->q_update_page_visit_info, 2,
				 page_id);
	rc = sqlite3_step (eh->priv->q_update_page_visit_info);
	sqlite3_reset (eh->priv->q_update_page_visit_info);
	ephy_db_return_on_error (eh->priv->db, rc, NULL);
	return TRUE;
}

static gboolean
add_page (EphyHistory * eh, const char *url, const char *title,
	  int visit_count, gint64 * page_id)
{
	int rc;
	gint64 site_id = 0;
	char *host_name;

	host_name = get_hostname_from_url (url);
	if (!get_site_id (eh, host_name, &site_id))
	{
		//g_debug("site not in table");
		rc = add_site (eh, host_name, &site_id);
	}
	g_free (host_name);

	rc = sqlite3_bind_text (eh->priv->q_add_page, 1, url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_bind_text (eh->priv->q_add_page, 2, title, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_bind_int (eh->priv->q_add_page, 3, visit_count);
	rc = sqlite3_bind_int64 (eh->priv->q_add_page, 4, site_id);
	rc = sqlite3_step (eh->priv->q_add_page);
	sqlite3_reset (eh->priv->q_add_page);
	if (rc != SQLITE_DONE)
		return FALSE;
	*page_id = sqlite3_last_insert_rowid (eh->priv->db);

	/* g_print("Add emit %llu %s\n", *page_id, url); */
	g_signal_emit (G_OBJECT (eh), signals[PAGE_ADDED], 0, *page_id);
	return TRUE;
}

static gboolean
add_site (EphyHistory * eh, const char *url, gint64 * site_id)
{
	int rc;

	rc = sqlite3_bind_text (eh->priv->q_add_site, 1, url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_step (eh->priv->q_add_site);
	sqlite3_reset (eh->priv->q_add_site);
	if (rc != SQLITE_DONE)
		return FALSE;
	*site_id = sqlite3_last_insert_rowid (eh->priv->db);
	return TRUE;
}

static gboolean
add_favicon (EphyHistory * eh, const char *url, gint64 * favicon_id)
{
	int rc;

	rc = sqlite3_bind_text (eh->priv->q_add_favicon, 1, url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_step (eh->priv->q_add_favicon);
	sqlite3_reset (eh->priv->q_add_favicon);
	if (rc != SQLITE_DONE)
		return FALSE;
	*favicon_id = sqlite3_last_insert_rowid (eh->priv->db);
	return TRUE;
}

static gboolean
add_visit (EphyHistory * eh, gint64 page_id, guint64 timestamp,
	   const char *ref_url, int trans_type, int visit_duration,
	   gint64 * visit_id)
{
	int rc;

	rc = sqlite3_bind_int64 (eh->priv->q_add_visit, 1, page_id);
	rc = sqlite3_bind_int64 (eh->priv->q_add_visit, 2, timestamp);
	rc = sqlite3_bind_text (eh->priv->q_add_visit, 3, ref_url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_bind_int (eh->priv->q_add_visit, 4, trans_type);
	rc = sqlite3_bind_int (eh->priv->q_add_visit, 5, visit_duration);
	rc = sqlite3_step (eh->priv->q_add_visit);
	sqlite3_reset (eh->priv->q_add_visit);
	if (rc != SQLITE_DONE)
		return FALSE;
	*visit_id = sqlite3_last_insert_rowid (eh->priv->db);

	/* XXX: emit PAGE_UPDATEE also/instead? */
	/* g_print("Update emit %llu\n", page_id); */
	g_signal_emit (G_OBJECT (eh), signals[PAGE_VISITED], 0, page_id);
	return TRUE;
}



/*
 * Public API
 */

/*
 * ephy-history compat API
 */

EphyHistory *
ephy_history_new (void)
{
	return EPHY_HISTORY (g_object_new (EPHY_TYPE_HISTORY, NULL));
}

gboolean
ephy_history_is_enabled (EphyHistory * eh)
{
	g_return_val_if_fail (EPHY_IS_HISTORY (eh), FALSE);

	return eh->priv->enabled;
}

int
ephy_history_get_page_visit_count (EphyHistory * eh, const char *url)
{
	gint64 page_id;
	int visit_count;

	get_page_visit_info (eh, url, &page_id, &visit_count);
	g_print ("get_visit_count: %s %u\n", url, visit_count);

	return visit_count;
}

GPtrArray *
ephy_history_get_hosts (EphyHistory * eh)
{
	GPtrArray *hosts;
	EphyHistoryPageNode *host = NULL;
	int rc;
	gint64 id, last_visit;
	guint visit_count;
	gchar *uri, *title, *favicon_uri;

	hosts = g_ptr_array_new ();
	while ((rc = sqlite3_step (eh->priv->q_get_hosts)) == SQLITE_ROW)
	{
		id = sqlite3_column_int64 (eh->priv->q_get_hosts, 0);
		uri = sqlite3_column_text (eh->priv->q_get_hosts, 1);
		title = sqlite3_column_text (eh->priv->q_get_hosts, 2);
		visit_count = sqlite3_column_int (eh->priv->q_get_hosts, 3);
		last_visit = sqlite3_column_int64 (eh->priv->q_get_hosts, 4);
		favicon_uri = sqlite3_column_text (eh->priv->q_get_hosts, 5);
		/*
		   g_debug("get_hosts: %llu %s %s %s %d %llu", id, uri, title,
		   favicon_uri, visit_count, last_visit);
		 */
		host = ephy_history_page_node_new (id, uri, title,
						   favicon_uri, visit_count,
						   last_visit);
		g_ptr_array_add (hosts, host);
	}
	if (rc != SQLITE_DONE)
	{
		/* XXX: right now we return whatever we got! */
	}
	sqlite3_reset (eh->priv->q_get_hosts);
	return hosts;
}

GPtrArray *
ephy_history_get_pages (EphyHistory * eh)
{
	GPtrArray *pages;
	EphyHistoryPageNode *page = NULL;
	int rc;
	gint64 id, site_id, last_visit;
	guint visit_count;
	gchar *uri, *title, *favicon_uri;

	pages = g_ptr_array_new ();
	while ((rc = sqlite3_step (eh->priv->q_get_pages)) == SQLITE_ROW)
	{
		id = sqlite3_column_int64 (eh->priv->q_get_pages, 0);
		uri = sqlite3_column_text (eh->priv->q_get_pages, 1);
		title = sqlite3_column_text (eh->priv->q_get_pages, 2);
		site_id = sqlite3_column_int64 (eh->priv->q_get_pages, 3);
		visit_count = sqlite3_column_int (eh->priv->q_get_pages, 4);
		last_visit = sqlite3_column_int64 (eh->priv->q_get_pages, 5);
		favicon_uri = sqlite3_column_text (eh->priv->q_get_pages, 6);
		/* 
		   g_debug("get_pages: %llu %s %s %s %d %llu", id, uri, title, favicon_uri,
		   visit_count, last_visit);       
		 */
		page = ephy_history_page_node_new (id, uri, title, site_id,
						   favicon_uri, visit_count,
						   last_visit);
		g_ptr_array_add (pages, page);
	}
	if (rc != SQLITE_DONE)
	{
		/* XXX: right now we return whatever we got! */
	}
	sqlite3_reset (eh->priv->q_get_pages);
	return pages;
}

gboolean
ephy_history_set_icon (EphyHistory * eh, const char *url,
		       const char *icon_url)
{
	gint64 icon_id = 0;
	int rc;

	if (!get_favicon_id (eh, icon_url, &icon_id))
	{
		//g_debug("icon not in table");
		rc = add_favicon (eh, icon_url, &icon_id);
	}
	rc = update_page_icon (eh, url, icon_id);

	return TRUE;
}

const char *
ephy_history_get_icon (EphyHistory * eh, const char *url)
{
	char *icon_url = NULL;
	int rc;

	rc = sqlite3_bind_text (eh->priv->q_get_favicon, 1, url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_step (eh->priv->q_get_favicon);
	if (rc == SQLITE_ROW)
		icon_url =
			g_strdup (sqlite3_column_text
				  (eh->priv->q_get_favicon, 0));

	rc = sqlite3_reset (eh->priv->q_get_favicon);
	return icon_url;
}

void
ephy_history_clear (EphyHistory * eh)
{
    /* XXX */
	g_signal_emit (eh, signals[HISTORY_CLEARED], 0);
}


/*
 * nsIGlobalHistory2 
 */

gboolean
ephy_history_add_page (EphyHistory * eh, const char *url, gboolean redirect,
		       gboolean toplevel, const char *ref_url)
{
	gint64 page_id;
	int visit_count;
	gint64 visit_id;

	if (eh->priv->enabled == FALSE)
	{
		return FALSE;
	}
	if (get_page_visit_info (eh, url, &page_id, &visit_count))
	{
		/* g_debug("%s page in table", url); */
		update_page_visit_info (eh, page_id, visit_count + 1);
	}
	/* not present */
	else
	{
		/* g_debug("%s page NOT in table", url); */
		add_page (eh, url, NULL, 1, &page_id);
	}
	add_visit (eh, page_id, get_current_time (), ref_url, 0, 0,
		   &visit_id);
	g_signal_emit (G_OBJECT (eh), signals[PAGE_VISITED], 0, url);

	return TRUE;
}

gboolean
ephy_history_is_page_visited (EphyHistory * eh, const char *url)
{
	gint64 page_id;
	int visit_count;

	if (get_page_visit_info (eh, url, &page_id, &visit_count)
	    && visit_count > 0)
		return TRUE;

	return FALSE;
}

gboolean
ephy_history_set_page_title (EphyHistory * eh, const char *url,
			     const char *title)
{
	/* XXX: don't update if title is same as the existing one */
	return update_page_title (eh, url, title);
}

/*
 * New stuff!
 */

/* XXX: Re-factor the following 4 funcs into 2 */

GArray *
ephy_history_get_page_ids (EphyHistory * eh)
{
	GArray *page_ids = NULL;
	char **ids;
	char *err_msg;
	int nrows;
	int ncols;
	guint64 id;
	int rc;
	int i;

	rc = sqlite3_get_table (eh->priv->db,
				"SELECT p.id FROM ephy_pages p ORDER BY id",
				&ids, &nrows, &ncols, &err_msg);
	if (rc == SQLITE_OK)
	{
		page_ids =
			g_array_sized_new (FALSE, FALSE, sizeof (guint64),
					   nrows);
		for (i = 1; i < nrows + 1; i++)
		{
			id = g_ascii_strtoull (ids[i], NULL, 10);
			g_array_append_val (page_ids, id);
		}
		sqlite3_free_table (ids);
	}
	else
	{
		g_critical ("get_page_ids: %s\n", err_msg);
		sqlite3_free (err_msg);
	}
	return page_ids;
}

EphyHistoryPageNode *
ephy_history_get_page_by_id (EphyHistory * eh, guint64 id)
{
	EphyHistoryPageNode *page = NULL;
	int rc;
	guint64 site_id, last_visit;
	guint visit_count;
	gchar *uri, *title, *favicon_uri;

	rc = sqlite3_bind_int64 (eh->priv->q_get_page_by_id, 1, id);
	rc = sqlite3_step (eh->priv->q_get_page_by_id);
	if (rc == SQLITE_ROW)
	{
		id = sqlite3_column_int64 (eh->priv->q_get_page_by_id, 0);
		uri = sqlite3_column_text (eh->priv->q_get_page_by_id, 1);
		title = sqlite3_column_text (eh->priv->q_get_page_by_id, 2);
		site_id = sqlite3_column_int64 (eh->priv->q_get_page_by_id, 3);
		visit_count =
			sqlite3_column_int (eh->priv->q_get_page_by_id, 4);
		last_visit =
			sqlite3_column_int64 (eh->priv->q_get_page_by_id, 5);
		favicon_uri =
			sqlite3_column_text (eh->priv->q_get_page_by_id, 6);
		page = ephy_history_page_node_new (id, uri, title, site_id,
						   favicon_uri, visit_count,
						   last_visit);
	}

	rc = sqlite3_reset (eh->priv->q_get_page_by_id);
	return page;
}

GArray *
ephy_history_get_site_ids (EphyHistory * eh)
{
	GArray *site_ids = NULL;
	char **ids;
	char *err_msg;
	int nrows;
	int ncols;
	guint64 id;
	int rc;
	int i;

	rc = sqlite3_get_table (eh->priv->db,
				"SELECT s.id FROM ephy_sites s ORDER BY id",
				&ids, &nrows, &ncols, &err_msg);
	if (rc == SQLITE_OK)
	{
        site_ids = g_array_sized_new (FALSE, FALSE, sizeof (guint64), nrows);
		for (i = 1; i < nrows + 1; i++)
		{
			id = g_ascii_strtoull (ids[i], NULL, 10);
			g_array_append_val (site_ids, id);
		}
		sqlite3_free_table (ids);
	}
	else
	{
		g_critical ("get_site_ids: %s\n", err_msg);
		sqlite3_free (err_msg);
	}
	return site_ids;
}

EphyHistoryPageNode *
ephy_history_get_site_by_id (EphyHistory * eh, guint64 id)
{
	EphyHistoryPageNode *site = NULL;
	int rc;
	gint64 last_visit = 0;
	guint visit_count = 0;
	gchar *uri, *title, *favicon_uri;

	rc = sqlite3_bind_int64 (eh->priv->q_get_site_by_id, 1, id);
	rc = sqlite3_step (eh->priv->q_get_site_by_id);
	if (rc == SQLITE_ROW)
	{
		id = sqlite3_column_int64 (eh->priv->q_get_site_by_id, 0);
		uri = sqlite3_column_text (eh->priv->q_get_site_by_id, 1);
		title = sqlite3_column_text (eh->priv->q_get_site_by_id, 2);
/* XXX
		visit_count =
			sqlite3_column_int (eh->priv->q_get_site_by_id, 3);
		last_visit =
			sqlite3_column_int64 (eh->priv->q_get_site_by_id, 4);
*/
		favicon_uri =
			sqlite3_column_text (eh->priv->q_get_site_by_id, 3);
		site = ephy_history_page_node_new (id, uri, title, favicon_uri, 
                            visit_count, last_visit);
	}

	rc = sqlite3_reset (eh->priv->q_get_site_by_id);
	return site;
}




/* XXX: incomplete */
gboolean 
ephy_history_remove_page (EphyHistory *eh, const char *url)
{
	guint64 id;
	int rc;

	rc = get_page_id (eh, url, &id);

	/* remove page */
	rc = sqlite3_bind_text (eh->priv->q_remove_page, 1, url, -1,
				SQLITE_TRANSIENT);
	rc = sqlite3_step (eh->priv->q_remove_page);
	rc = sqlite3_reset (eh->priv->q_remove_page);

	/* remove page visits */
	/* XXX: use triggers to maintain page_visits->pages 
 	 * foreign key referential integrity? */
	rc = sqlite3_bind_int64 (eh->priv->q_remove_page_visits, 1, id);
	rc = sqlite3_step (eh->priv->q_remove_page_visits);
	rc = sqlite3_reset (eh->priv->q_remove_page_visits);

	/* g_print("Remove emit %llu %s\n", id, url); */
	g_signal_emit (G_OBJECT (eh), signals[PAGE_REMOVED], 0, id);
	return TRUE;
}
