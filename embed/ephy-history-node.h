#ifndef EPHY_HISTORY_NODE_H
#define EPHY_HISTORY_NODE_H

#include <glib-object.h>

G_BEGIN_DECLS

#define EPHY_TYPE_HISTORY_NODE          (ephy_history_node_get_type ())

#define EPHY_HISTORY_NODE(o)            (G_TYPE_CHECK_INSTANCE_CAST ((o), EPHY_TYPE_HISTORY_NODE, EphyHistoryNode))

#define EPHY_HISTORY_NODE_CLASS(k)      (G_TYPE_CHECK_CLASS_CAST((k), EPHY_TYPE_HISTORY_NODE, EphyHistoryNodeClass))

#define EPHY_IS_HISTORY_NODE(o)         (G_TYPE_CHECK_INSTANCE_TYPE ((o), EPHY_TYPE_HISTORY_NODE))

#define EPHY_IS_HISTORY_NODE_CLASS(k)   (G_TYPE_CHECK_CLASS_TYPE ((k), EPHY_TYPE_HISTORY_NODE))

#define EPHY_HISTORY_NODE_GET_CLASS(o)  (G_TYPE_INSTANCE_GET_CLASS ((o), EPHY_TYPE_HISTORY_NODE, EphyHistoryNodeClass))

typedef struct _EphyHistoryNodeClass    EphyHistoryNodeClass;
typedef struct _EphyHistoryNode         EphyHistoryNode;
typedef struct _EphyHistoryNodePrivate  EphyHistoryNodePrivate;

enum
{
    /* Base node props */
    EPHY_HISTORY_NODE_PROP_ID = 1,
    EPHY_HISTORY_NODE_PROP_URL,
    EPHY_HISTORY_NODE_PROP_TITLE,
    EPHY_HISTORY_NODE_PROP_FAVICON_URL,
    /* Page node props */
    EPHY_HISTORY_PAGE_NODE_PROP_SITE_ID,
    EPHY_HISTORY_PAGE_NODE_PROP_VISIT_COUNT,
    EPHY_HISTORY_PAGE_NODE_PROP_LAST_VISIT,
    /* Page visit props */
    EPHY_HISTORY_VISIT_NODE_PROP_DATE,
    EPHY_HISTORY_VISIT_NODE_PROP_TYPE,
    EPHY_HISTORY_VISIT_NODE_PROP_DURATION,
    EPHY_HISTORY_VISIT_NODE_PROP_REF_ID,
};

struct _EphyHistoryNode
{
    GObject parent;

    /*< private >*/
    EphyHistoryNodePrivate *priv;
};

struct _EphyHistoryNodeClass
{
    GObjectClass parent_class;
};

GType ephy_history_node_get_type (void);

/* XXX: compat functions to get/set properties by property ID instead of name 
 * Re-write and remove or move them someplace better
 **/
void ephy_history_node_get_property_by_id (EphyHistoryNode *node, 
					    guint prop_id, GValue **value);

void ephy_history_node_set_property_by_id (EphyHistoryNode *node, 
					    guint prop_id, const GValue *value);

G_END_DECLS

#endif /* EPHY_HISTORY_NODE_H */
